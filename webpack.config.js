const path = require('path')
const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const HtmlWebpackHarddiskPlugin = require('html-webpack-harddisk-plugin')

const devMode = process.env.NODE_ENV === 'development'

const plugins = [
  new HtmlWebpackPlugin({
    inject: false,
    template: `${__dirname}/resources/views/index.html`,
    filename: `${__dirname}/resources/views/index.blade.php`,
    alwaysWriteToDisk: true,
  }),
  new HtmlWebpackHarddiskPlugin({}),
]
const devPlugins = [
  new webpack.NamedModulesPlugin(),
  new webpack.NamedChunksPlugin(),
  new webpack.HotModuleReplacementPlugin(),
]

module.exports = {
  mode: devMode ? 'development' : 'production',
  entry: {
    app: ['react-hot-loader/patch', './resources/app/index.tsx'],
    events: 'utils/events.ts',
  },
  output: {
    path: `${__dirname}/public/assets`,
    publicPath: devMode ? 'http://localhost:8080/' : '/assets/',
    filename: devMode ? '[name].js' : '[name].[contenthash:7].js',
    chunkFilename: devMode ? '[name].js' : '[name].[contenthash:7].js',
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        loader: 'ts-loader',
        options: {
          transpileOnly: true,
        },
      },
      {
        test: /\.css?$/,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.(png|jpg|gif)$/,
        loader: 'url-loader',
        options: {
          limit: 8192,
        },
      },
      {
        test: /\.(svg|woff2?|eot|ttf)$/,
        loader: devMode ? 'url-loader' : 'file-loader',
      },
    ],
  },
  plugins: devMode ? plugins.concat(devPlugins) : plugins,
  resolve: {
    extensions: ['.tsx', '.ts', '.js'],
    alias: {
      'react-dom': '@hot-loader/react-dom',
      '~': path.resolve(__dirname, 'resources/app'),
      utils: path.resolve(__dirname, 'resources/utils'),
    },
  },
  externals: {
    react: 'React',
  },
  devtool: devMode ? 'cheap-module-eval-source-map' : false,
  devServer: {
    headers: {
      'Access-Control-Allow-Origin': '*',
    },
    host: '0.0.0.0',
    hot: true,
    hotOnly: true,
    stats: 'errors-only',
  },
  stats: 'errors-only',
}
