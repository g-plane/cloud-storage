import React from 'react'
import {
  StylesProvider,
  createGenerateClassName,
} from '@material-ui/core/styles'
import ShareIcon from '@material-ui/icons/Share'
import type { RouteComponentProps } from '@reach/router'
import * as fetch from '../../../../resources/utils/fetch'
import type { File } from '../../../../resources/app/types'
import ShareList from './ShareList'

type FileAction = {
  icon: React.ReactNode
  tooltip: string
  handler(event: React.MouseEvent): void
}

type Navigation = {
  link: string
  title: string
  icon: React.ReactElement
}

const generateClassName = createGenerateClassName({
  disableGlobal: true,
})

const Shares: React.FC<RouteComponentProps> = () => {
  return (
    <StylesProvider generateClassName={generateClassName}>
      <ShareList />
    </StylesProvider>
  )
}

events.on(
  'pagesOfHome',
  ({ components }: { components: React.ReactNodeArray }) => {
    components.push(<Shares path="share" />)
  },
)

events.on(
  'fileActions',
  ({ actions, file }: { actions: FileAction[]; file: File }) => {
    if (file.type === 'directory') {
      return
    }

    actions.push({
      icon: <ShareIcon />,
      tooltip: '分享',
      async handler() {
        type Ok = { code: 0; link: string }
        type Err = { code: 1; message: string }
        const resp: Ok | Err = await fetch.post(`/share/${file.id}`)
        if (resp.code === 0) {
          const input = document.createElement('input')
          input.style.visibility = 'none'
          input.value = resp.link
          document.body.appendChild(input)
          input.select()
          document.execCommand('copy')
          input.remove()
          alert('分享成功。链接已复制到剪贴板。')
        } else {
          alert(resp.message)
        }
      },
    })
  },
)

events.on(
  'homeLeftDrawer',
  ({ navigations }: { navigations: Navigation[] }) => {
    navigations.push({
      link: '/home/share',
      title: '我的分享',
      icon: <ShareIcon />,
    })
  },
)
