import React, { useState, useEffect } from 'react'
import Box from '@material-ui/core/Box'
import Button from '@material-ui/core/Button'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import CircularProgress from '@material-ui/core/CircularProgress'
import Container from '@material-ui/core/Container'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction'
import ListItemText from '@material-ui/core/ListItemText'
import Typography from '@material-ui/core/Typography'
import * as fetch from '../../../../resources/utils/fetch'

type Share = {
  id: number
  user_id: number
  file_id: number
  code: string
  times: number
  name: string
  link: string
  created_at: string
}

const ShareList: React.FC = () => {
  const [shares, setShares] = useState<Share[]>([])
  const [isLoading, setIsLoading] = useState(false)

  useEffect(() => {
    const getShares = async () => {
      setIsLoading(true)
      const shares: Share[] = await fetch.get('/share')
      setShares(shares)
      setIsLoading(false)
    }
    getShares()
  }, [])

  const handleDelete = async (share: Share) => {
    const confirmed = confirm('确认要取消该分享吗？')
    if (!confirmed) {
      return
    }

    const { id } = share
    const {
      code,
      message,
    }: { code: number; message: string } = await fetch.del(`/share/${id}`)
    if (code === 0) {
      setShares((shares) => shares.filter((share) => share.id !== id))
    } else {
      alert(message)
    }
  }

  return (
    <Container maxWidth="md">
      {isLoading ? (
        <Box display="flex" justifyContent="center">
          <CircularProgress />
        </Box>
      ) : shares.length === 0 ? (
        <Typography align="center">您未分享过任何文件</Typography>
      ) : (
        <Card>
          <CardContent>
            <Typography variant="h6">分享列表</Typography>
            <List>
              {shares.map((share) => (
                <ListItem key={share.id}>
                  <ListItemText
                    secondary={`下载次数：${share.times}，URL: ${share.link}`}
                  >
                    {share.name}
                  </ListItemText>
                  <ListItemSecondaryAction>
                    <Button
                      color="secondary"
                      onClick={() => handleDelete(share)}
                    >
                      取消分享
                    </Button>
                  </ListItemSecondaryAction>
                </ListItem>
              ))}
            </List>
          </CardContent>
        </Card>
      )}
    </Container>
  )
}

export default ShareList
