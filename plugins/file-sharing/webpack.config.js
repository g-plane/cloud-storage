const devMode = process.env.NODE_ENV === 'development'

module.exports = {
  mode: devMode ? 'development' : 'production',
  entry: `${__dirname}/assets/src/index.tsx`,
  output: {
    path: `${__dirname}/assets/dist`,
    publicPath: '/plugins/file-sharing/assets/dist/',
    filename: 'index.js',
    chunkFilename: devMode ? '[name].js' : '[name].[contenthash:7].js',
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        loader: 'ts-loader',
        options: {
          transpileOnly: true,
        },
      },
      {
        test: /\.css?$/,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.(png|jpg|gif)$/,
        loader: 'url-loader',
        options: {
          limit: 8192,
        },
      },
      {
        test: /\.(svg|woff2?|eot|ttf)$/,
        loader: devMode ? 'url-loader' : 'file-loader',
      },
    ],
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.js'],
  },
  externals: {
    react: 'React',
  },
  devtool: devMode ? 'cheap-module-eval-source-map' : false,
  stats: 'errors-only',
}
