<?php

namespace FileSharing\Controllers;

use App\Http\Controllers\Controller;
use App\Models\File;
use FileSharing\Models\Share;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class ShareController extends Controller
{
    public function list()
    {
        return Share::where('user_id', auth()->id())
            ->get()
            ->map(function (Share $share) {
                $share->link = url('s/'.$share->code);
                $share->name = $share->file->name;

                return $share;
            });
    }

    public function create($id)
    {
        /** @var File|null */
        $file = File::find($id);
        if (empty($file)) {
            return response()->json(['code' => 1, 'message' => '文件不存在']);
        }
        if ($file->user_id !== auth()->id()) {
            return response()->json(['code' => 1, 'message' => '您没有权限']);
        }
        if ($file->type === 'directory') {
            return response()->json(['code' => 1, 'message' => '文件夹不能被分享']);
        }

        if (Share::where('file_id', $id)->count() > 0) {
            return response(['code' => 1, 'message' => '已经分享过']);
        }

        $share = new Share();
        $share->user_id = auth()->id();
        $share->file_id = $file->id;
        $share->code = Str::random();
        $share->save();

        return response(['code' => 0, 'link' => url('s/'.$share->code)]);
    }

    public function download(Dispatcher $dispatcher, $code)
    {
        /** @var Share */
        $share = Share::where('code', $code)->firstOrFail();

        /** @var File|null */
        $file = $share->file;
        abort_if(empty($file), 404);

        $share->times += 1;
        $share->save();

        $name = $file->name;
        $dispatcher->dispatch('file.download', [$file, $name]);

        return Storage::download($file->path, $name, [
            'Content-Type' => $file->type,
        ]);
    }

    public function delete($id)
    {
        /** @var Share|null */
        $share = Share::find($id);
        if (empty($share)) {
            return response()->json(['code' => 1, 'message' => '分享不存在']);
        }
        if ($share->user_id !== auth()->id()) {
            return response()->json(['code' => 1, 'message' => '您没有权限']);
        }

        $share->delete();

        return response()->json(['code' => 0, 'message' => '分享已取消']);
    }
}
