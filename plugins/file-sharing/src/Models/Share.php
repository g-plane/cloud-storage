<?php

namespace FileSharing\Models;

use App\Models\File;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $user_id
 * @property int $file_id
 * @property string $code
 * @property int $times
 * @property File $file
 */
class Share extends Model
{
    protected $casts = [
        'user_id' => 'integer',
        'file_id' => 'integer',
        'times' => 'integer',
    ];

    public function file()
    {
        return $this->belongsTo(File::class);
    }
}
