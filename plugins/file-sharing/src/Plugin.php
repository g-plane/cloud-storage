<?php

namespace FileSharing;

use App\Services\PluginManager;
use Blessing\Filter;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Plugin
{
    public function boot(
        PluginManager $pluginManager,
        Filter $filter,
        Dispatcher $dispatcher
    ) {
        $version = $pluginManager->getManifest('file-sharing')['version'];

        $filter->add('javascript', function ($scripts) use ($version) {
            $scripts[] = url('plugins/file-sharing/assets/dist/index.js?v='.$version);

            return $scripts;
        });

        $dispatcher->listen('routes.registering', Listeners\RegisterRoutes::class);
        $dispatcher->listen('file.delete.after', Listeners\DeleteShare::class);
    }

    public function enable()
    {
        if (!Schema::hasTable('shares')) {
            Schema::create('shares', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->bigInteger('user_id');
                $table->bigInteger('file_id');
                $table->string('code');
                $table->bigInteger('times')->default(0);
                $table->timestamps();
            });
        }
    }
}
