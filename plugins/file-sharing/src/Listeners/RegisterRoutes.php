<?php

namespace FileSharing\Listeners;

use Illuminate\Support\Facades\Route;

class RegisterRoutes
{
    public function handle()
    {
        Route::namespace('FileSharing\Controllers')->group(function () {
            Route::get('s/{code}', 'ShareController@download');

            Route::middleware('auth')->prefix('share')->group(function () {
                Route::get('', 'ShareController@list');
                Route::post('{id}', 'ShareController@create');
                Route::delete('{id}', 'ShareController@delete');
            });
        });
    }
}
