<?php

namespace FileSharing\Listeners;

use FileSharing\Models\Share;

class DeleteShare
{
    public function handle($file)
    {
        Share::where('file_id', $file->id)->delete();
    }
}
