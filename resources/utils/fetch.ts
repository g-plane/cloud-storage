import cookie from 'cookie'

const init: RequestInit = {
  mode: 'same-origin',
  redirect: 'follow',
  credentials: 'same-origin',
}

export function getXsrfToken(): string {
  return cookie.parse(document.cookie)['XSRF-TOKEN']
}

function headers() {
  return {
    Accept: 'application/json',
    'Content-Type': 'application/json',
    'X-XSRF-TOKEN': getXsrfToken(),
  }
}

function processResponse(response: Response) {
  if (response.headers.get('Content-Type') === 'application/json') {
    return response.json()
  } else {
    return response.text()
  }
}

export function send(method: string, url: string, body = '') {
  return fetch(url, {
    method: method.toUpperCase(),
    headers: headers(),
    body: method.toUpperCase() === 'GET' ? undefined : body,
    ...init,
  })
}

export async function get(url: string, parameters = {}) {
  const search = new URLSearchParams(parameters)
  const resp = await send('get', `${url}?${search}`)

  return processResponse(resp)
}

export async function post(url: string, body = {}) {
  const resp = await send('post', url, JSON.stringify(body))

  return processResponse(resp)
}

export async function put(url: string, body = {}) {
  const resp = await send('put', url, JSON.stringify(body))

  return processResponse(resp)
}

export async function del(url: string, body = {}) {
  const resp = await send('delete', url, JSON.stringify(body))

  return processResponse(resp)
}
