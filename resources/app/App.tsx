import React, { useEffect } from 'react'
import { hot } from 'react-hot-loader/root'
import 'typeface-roboto'
import CssBaseline from '@material-ui/core/CssBaseline'
import { Router, RouteComponentProps } from '@reach/router'
import { useImmer } from 'use-immer'
import * as fetch from 'utils/fetch'
import { User } from './types'
import AppContext, { defaultAppState } from '~/shared/AppContext'
import useExternalComponents from '~/shared/useExternalComponents'
import Home from './home/Home'
import Admin from './admin/Base'

const asyncComponent = (dynImport: () => Promise<{ default: React.FC }>) => {
  const Async = React.lazy(dynImport)

  const component: React.FC<RouteComponentProps> = () => (
    <React.Suspense fallback={''}>
      <Async />
    </React.Suspense>
  )

  return component
}

const Login = asyncComponent(() => import('./auth/Login'))
const Register = asyncComponent(() => import('./auth/Register'))
const Profile = asyncComponent(() => import('./home/Profile'))
const Files = asyncComponent(() => import('./home/Files'))
const Trash = asyncComponent(() => import('./home/Trash'))
const UsersManagement = asyncComponent(() =>
  import('./admin/users/UsersManagement'),
)
const PluginsManagement = asyncComponent(() =>
  import('./admin/plugins/PluginsManagement'),
)

const App: React.FC = () => {
  const [app, setApp] = useImmer(defaultAppState)
  const pagesOfRoot = useExternalComponents('pagesOfRoot')
  const pagesOfHome = useExternalComponents('pagesOfHome')
  const pagesOfAdmin = useExternalComponents('pagesOfAdmin')

  useEffect(() => {
    const getUser = async () => {
      const resp = await fetch.send('get', '/home/user')
      if (resp.ok) {
        const user: User = await resp.json()
        setApp((app) => {
          app.user = user
        })
      }
    }
    getUser()
  }, [])

  return (
    <AppContext.Provider value={[app, setApp]}>
      <CssBaseline />
      <Router>
        <Login path="auth/login" />
        <Register path="auth/register" />
        <Home path="home">
          <Files path="/" />
          <Trash path="trash" />
          <Profile path="profile" />
          {pagesOfHome}
        </Home>
        {app.user.role === 'admin' && (
          <Admin path="admin">
            <UsersManagement path="users" />
            <PluginsManagement path="plugins" />
            {pagesOfAdmin}
          </Admin>
        )}
        {pagesOfRoot}
      </Router>
    </AppContext.Provider>
  )
}

export default hot(App)
