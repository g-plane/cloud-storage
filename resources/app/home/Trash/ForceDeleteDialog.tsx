import React from 'react'
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import * as fetch from 'utils/fetch'
import { File } from '~/types'
import useSnackbar from '~/shared/useSnackbar'

interface Props {
  open: boolean
  file: File | null
  onDelete(file: File): void
  onClose(): void
}

const ForceDeleteDialog: React.FC<Props> = (props) => {
  const { open, file, onDelete, onClose } = props

  const [Snackbar, setShowSnackbar] = useSnackbar()

  const handleDelete = async () => {
    if (file === null) {
      return
    }

    const {
      code,
      message,
    }: {
      code: number
      message: string
    } = await fetch.del('/trash/force-delete', { id: file.id })

    setShowSnackbar(message)
    if (code === 0) {
      onDelete(file)
    }
    onClose()
  }

  if (file === null) {
    return null
  }

  return (
    <>
      <Dialog open={open} onClose={onClose}>
        <DialogTitle>彻底删除</DialogTitle>
        <DialogContent>
          <DialogContentText>
            确定要彻底删除 {file.name}{' '}
            {file.type === 'directory' && '及其子文件和文件夹'}
            吗？此操作不可撤销。
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button color="primary" onClick={handleDelete}>
            确定
          </Button>
          <Button color="primary" onClick={onClose}>
            取消
          </Button>
        </DialogActions>
      </Dialog>
      <Snackbar />
    </>
  )
}

export default ForceDeleteDialog
