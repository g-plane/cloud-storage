import React, { useState, useEffect, useContext } from 'react'
import { hot } from 'react-hot-loader/root'
import { makeStyles } from '@material-ui/core/styles'
import Box from '@material-ui/core/Box'
import Button from '@material-ui/core/Button'
import CircularProgress from '@material-ui/core/CircularProgress'
import Container from '@material-ui/core/Container'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import IconButton from '@material-ui/core/IconButton'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction'
import ListItemText from '@material-ui/core/ListItemText'
import Tooltip from '@material-ui/core/Tooltip'
import Typography from '@material-ui/core/Typography'
import DeleteForeverIcon from '@material-ui/icons/DeleteForever'
import FolderIcon from '@material-ui/icons/Folder'
import InsertDriveFileIcon from '@material-ui/icons/InsertDriveFile'
import RestoreIcon from '@material-ui/icons/Restore'
import * as fetch from 'utils/fetch'
import { File } from '~/types'
import useSnackbar from '~/shared/useSnackbar'
import AppContext from '~/shared/AppContext'
import ForceDeleteDialog from './ForceDeleteDialog'

const useStyles = makeStyles(() => ({
  iconButton: {
    marginRight: '1px',
  },
}))

const TrashBin: React.FC = () => {
  const [files, setFiles] = useState<File[]>([])
  const [isLoading, setIsLoading] = useState(false)
  const [forceDeleting, setForceDeleting] = useState<File | null>(null)
  const [showClearConfirmDialog, setShowClearConfirmDialog] = useState(false)
  const [, setApp] = useContext(AppContext)
  const classes = useStyles()
  const [Snackbar, setShowMessage] = useSnackbar()

  useEffect(() => {
    const getTrashList = async () => {
      setIsLoading(true)
      const files: File[] = await fetch.get('/trash/list')
      setFiles(files)
      setIsLoading(false)
    }
    getTrashList()
  }, [])

  const handleRestore = async (item: File) => {
    const {
      code,
      message,
      size,
    }: {
      code: number
      message: string
      size: number
    } = await fetch.del('/trash/restore', { id: item.id })
    setShowMessage(message)
    if (code === 0) {
      setFiles((files) => files.filter((file) => file.id !== item.id))
      setApp((app) => {
        app.user.used_space += size
      })
    }
  }

  const handleForceDelete = (item: File) => {
    setFiles((files) => files.filter((file) => file.id !== item.id))
  }

  const handleCloseForceDeleteDialog = () => setForceDeleting(null)

  const handleOpenClearConfirmDialog = () => setShowClearConfirmDialog(true)
  const handleCloseClearConfirmDialog = () => setShowClearConfirmDialog(false)

  const handleClearTrashBin = async () => {
    const {
      code,
      message,
    }: { code: number; message: string } = await fetch.del('/trash/clear')

    if (code === 0) {
      setShowClearConfirmDialog(false)
      setFiles([])
    } else {
      setShowMessage(message)
    }
  }

  return (
    <>
      <Container maxWidth="xl">
        <Box display="flex" justifyContent="space-between">
          {files.length > 0 ? (
            <Button
              startIcon={<DeleteForeverIcon />}
              color="secondary"
              onClick={handleOpenClearConfirmDialog}
            >
              清空回收站
            </Button>
          ) : (
            <div></div>
          )}
          <Typography variant="caption">
            提示：系统将自动清理回收站中存放超过 10 天的文件或文件夹。
          </Typography>
        </Box>
        {isLoading ? (
          <Box display="flex" justifyContent="center" mt={3}>
            <CircularProgress />
          </Box>
        ) : files.length === 0 ? (
          <Box display="flex" justifyContent="center" mt={3}>
            <Typography>回收站是空的</Typography>
          </Box>
        ) : (
          <List>
            {files.map((file) => {
              const date = new Date(file.deleted_at!).toLocaleString()

              return (
                <ListItem key={file.id}>
                  <ListItemIcon>
                    {file.type === 'directory' ? (
                      <FolderIcon />
                    ) : (
                      <InsertDriveFileIcon />
                    )}
                  </ListItemIcon>
                  <ListItemText secondary={`删除于 ${date}`}>
                    {file.name}
                  </ListItemText>
                  <ListItemSecondaryAction>
                    <Tooltip title="还原">
                      <IconButton
                        edge="end"
                        className={classes.iconButton}
                        onClick={() => handleRestore(file)}
                      >
                        <RestoreIcon />
                      </IconButton>
                    </Tooltip>
                    <Tooltip title="彻底删除">
                      <IconButton
                        edge="end"
                        className={classes.iconButton}
                        onClick={() => setForceDeleting(file)}
                      >
                        <DeleteForeverIcon />
                      </IconButton>
                    </Tooltip>
                  </ListItemSecondaryAction>
                </ListItem>
              )
            })}
          </List>
        )}
      </Container>
      <Snackbar />
      <ForceDeleteDialog
        open={!!forceDeleting}
        file={forceDeleting}
        onDelete={handleForceDelete}
        onClose={handleCloseForceDeleteDialog}
      />
      <Dialog
        open={showClearConfirmDialog}
        onClose={handleCloseClearConfirmDialog}
      >
        <DialogTitle>清空回收站</DialogTitle>
        <DialogContent>
          <DialogContentText>
            确定要清空回收站吗？此操作不可撤销。
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button color="primary" onClick={handleClearTrashBin}>
            确定
          </Button>
          <Button color="primary" onClick={handleCloseClearConfirmDialog}>
            取消
          </Button>
        </DialogActions>
      </Dialog>
    </>
  )
}

export default hot(TrashBin)
