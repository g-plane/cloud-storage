import React, { useState, useEffect, useRef, useContext } from 'react'
import { hot } from 'react-hot-loader/root'
import Button from '@material-ui/core/Button'
import CloudUploadIcon from '@material-ui/icons/CloudUpload'
import Uppy from '@uppy/core'
import { DashboardModal } from '@uppy/react'
import XHRUpload from '@uppy/xhr-upload'
import uppyLocale from '@uppy/locales/lib/zh_CN'
import '@uppy/core/dist/style.css'
import '@uppy/dashboard/dist/style.css'
import * as fetch from 'utils/fetch'
import { File } from '~/types'
import AppContext from '~/shared/AppContext'
import useSnackbar from '~/shared/useSnackbar'

interface Props {
  onUpload(file: File): void
}

const Upload: React.FC<Props> = (props) => {
  const [open, setOpen] = useState(false)
  const [app] = useContext(AppContext)
  const [Snackbar, setShowSnackbar] = useSnackbar()
  const uppy = useRef<Uppy.Uppy>()

  useEffect(() => {
    uppy.current = Uppy({ locale: uppyLocale })
    uppy.current
      .use(XHRUpload, {
        endpoint: '/files/upload',
        fieldName: 'file',
        headers: {
          'X-XSRF-TOKEN': fetch.getXsrfToken(),
        },
        getResponseError(responseText: string) {
          setShowSnackbar(JSON.parse(responseText).message)
        },
      })
      .on('file-added', () => {
        uppy.current!.setMeta({
          location: app.location.current.id,
        })
      })
      .on('upload-success', (_, response) => {
        props.onUpload(response.body.file as File)
      })

    return () => {
      uppy.current?.close()
    }
  }, [app.location.current.id])

  const handleOpenButtonClick = () => setOpen(true)

  const handleRequestClose = () => setOpen(false)

  return (
    <>
      <Button
        startIcon={<CloudUploadIcon />}
        color="secondary"
        onClick={handleOpenButtonClick}
      >
        上传文件
      </Button>
      {uppy.current && (
        <DashboardModal
          uppy={uppy.current}
          open={open}
          onRequestClose={handleRequestClose}
          closeModalOnClickOutside
        />
      )}
      <Snackbar />
    </>
  )
}

export default hot(Upload)
