import React, { useState, useContext } from 'react'
import Box from '@material-ui/core/Box'
import Button from '@material-ui/core/Button'
import Paper from '@material-ui/core/Paper'
import TextField from '@material-ui/core/TextField'
import Typography from '@material-ui/core/Typography'
import * as fetch from 'utils/fetch'
import AppContext from '~/shared/AppContext'
import useSnackbar from '~/shared/useSnackbar'
import useStyles from './styles'

const Name: React.FC = () => {
  const [app, setApp] = useContext(AppContext)
  const [name, setName] = useState(app.user.name)
  const [Snackbar, setShowSnackbar] = useSnackbar()
  const classes = useStyles()

  const handleNameChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setName(event.target.value)
  }

  const handleSubmit = async () => {
    const {
      code,
      message,
    }: { code: number; message: string } = await fetch.put('/profile/name', {
      name,
    })

    setShowSnackbar(message)
    if (code === 0) {
      setApp((app) => {
        app.user.name = name
      })
    }
  }

  return (
    <Paper>
      <Box p={2}>
        <Typography variant="h6">修改用户名</Typography>
        <Box mt={2}>
          <TextField
            label="用户名"
            type="text"
            variant="outlined"
            className={classes.input}
            value={name}
            onChange={handleNameChange}
          />
        </Box>
        <Box mt={3}>
          <Button variant="contained" color="secondary" onClick={handleSubmit}>
            提交
          </Button>
        </Box>
      </Box>
      <Snackbar />
    </Paper>
  )
}

export default Name
