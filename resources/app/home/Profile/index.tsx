import React from 'react'
import { hot } from 'react-hot-loader/root'
import Box from '@material-ui/core/Box'
import Button from '@material-ui/core/Button'
import Container from '@material-ui/core/Container'
import ArrowBackIcon from '@material-ui/icons/ArrowBack'
import { useNavigate } from '@reach/router'
import Email from './Email'
import Name from './Name'
import Password from './Password'

const Profile: React.FC = () => {
  const navigate = useNavigate()

  const handleBack = () => {
    navigate('/home')
  }

  return (
    <Container maxWidth="md">
      <div>
        <Button
          startIcon={<ArrowBackIcon />}
          color="secondary"
          onClick={handleBack}
        >
          返回首页
        </Button>
      </div>
      <Box my={1}>
        <Email />
      </Box>
      <Box my={1}>
        <Name />
      </Box>
      <Box my={1}>
        <Password />
      </Box>
    </Container>
  )
}

export default hot(Profile)
