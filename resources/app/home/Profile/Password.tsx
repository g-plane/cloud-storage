import React, { useState } from 'react'
import Box from '@material-ui/core/Box'
import Button from '@material-ui/core/Button'
import Paper from '@material-ui/core/Paper'
import TextField from '@material-ui/core/TextField'
import Typography from '@material-ui/core/Typography'
import * as fetch from 'utils/fetch'
import useSnackbar from '~/shared/useSnackbar'
import useStyles from './styles'

const Password: React.FC = () => {
  const [old, setOld] = useState('')
  const [password, setPassword] = useState('')
  const [confirmation, setConfirmation] = useState('')
  const [Snackbar, setShowSnackbar] = useSnackbar()
  const classes = useStyles()

  const handleOldChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setOld(event.target.value)
  }

  const handlePasswordChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setPassword(event.target.value)
  }

  const handleConfirmationChange = (
    event: React.ChangeEvent<HTMLInputElement>,
  ) => {
    setConfirmation(event.target.value)
  }

  const handleSubmit = async () => {
    const { message }: { message: string } = await fetch.put(
      '/profile/password',
      {
        old,
        password,
        password_confirmation: confirmation,
      },
    )

    setShowSnackbar(message)
  }

  return (
    <Paper>
      <Box p={2}>
        <Typography variant="h6">修改密码</Typography>
        <Box mt={2}>
          <TextField
            label="旧密码"
            type="password"
            variant="outlined"
            className={classes.input}
            value={old}
            onChange={handleOldChange}
          />
        </Box>
        <Box mt={2}>
          <TextField
            label="新密码"
            type="password"
            variant="outlined"
            className={classes.input}
            value={password}
            onChange={handlePasswordChange}
          />
        </Box>
        <Box mt={2}>
          <TextField
            label="新密码确认"
            type="password"
            variant="outlined"
            className={classes.input}
            value={confirmation}
            onChange={handleConfirmationChange}
          />
        </Box>
        <Box mt={3}>
          <Button variant="contained" color="secondary" onClick={handleSubmit}>
            提交
          </Button>
        </Box>
      </Box>
      <Snackbar />
    </Paper>
  )
}

export default Password
