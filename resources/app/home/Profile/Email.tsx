import React, { useState, useContext } from 'react'
import Box from '@material-ui/core/Box'
import Button from '@material-ui/core/Button'
import Paper from '@material-ui/core/Paper'
import TextField from '@material-ui/core/TextField'
import Typography from '@material-ui/core/Typography'
import * as fetch from 'utils/fetch'
import AppContext from '~/shared/AppContext'
import useSnackbar from '~/shared/useSnackbar'
import useStyles from './styles'

const Email: React.FC = () => {
  const [app, setApp] = useContext(AppContext)
  const [email, setEmail] = useState(app.user.email)
  const [password, setPassword] = useState('')
  const [Snackbar, setShowSnackbar] = useSnackbar()
  const classes = useStyles()

  const handleEmailChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setEmail(event.target.value)
  }

  const handlePasswordChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setPassword(event.target.value)
  }

  const handleSubmit = async () => {
    const {
      code,
      message,
    }: { code: number; message: string } = await fetch.put('/profile/email', {
      email,
      password,
    })

    setShowSnackbar(message)
    if (code === 0) {
      setApp((app) => {
        app.user.email = email
      })
    }
  }

  return (
    <Paper>
      <Box p={2}>
        <Typography variant="h6">修改邮箱</Typography>
        <Box mt={2}>
          <TextField
            label="邮箱地址"
            type="email"
            variant="outlined"
            className={classes.input}
            value={email}
            onChange={handleEmailChange}
          />
        </Box>
        <Box mt={2}>
          <TextField
            label="密码"
            type="password"
            variant="outlined"
            className={classes.input}
            value={password}
            onChange={handlePasswordChange}
          />
        </Box>
        <Box mt={3}>
          <Button variant="contained" color="secondary" onClick={handleSubmit}>
            提交
          </Button>
        </Box>
      </Box>
      <Snackbar />
    </Paper>
  )
}

export default Email
