import React, { useState, useContext } from 'react'
import { hot } from 'react-hot-loader/root'
import { makeStyles, Theme } from '@material-ui/core/styles'
import AppBar from '@material-ui/core/AppBar'
import Box from '@material-ui/core/Box'
import Button from '@material-ui/core/Button'
import Container from '@material-ui/core/Container'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import CloudIcon from '@material-ui/icons/Cloud'
import AccountCircleIcon from '@material-ui/icons/AccountCircle'
import { RouteComponentProps } from '@reach/router'
import AppContext from '~/shared/AppContext'
import LeftDrawer from './LeftDrawer'
import RightDrawer from '~/shared/RightDrawer'

const useStyles = makeStyles((theme: Theme) => ({
  container: {
    display: 'flex',
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  },
  icon: {
    marginRight: '5px',
  },
  userButton: {
    color: 'white',
  },
  toolbar: theme.mixins.toolbar,
}))

const Home: React.FC<RouteComponentProps> = (props) => {
  const [showUserDrawer, setShowUserDrawer] = useState(false)
  const [app] = useContext(AppContext)
  const classes = useStyles()

  const openUserDrawer = () => setShowUserDrawer(true)
  const closeUserDrawer = () => setShowUserDrawer(false)

  return (
    <Container maxWidth="xl" disableGutters className={classes.container}>
      <AppBar position="fixed" className={classes.appBar}>
        <Toolbar>
          <CloudIcon className={classes.icon} />
          <Typography variant="h6">我的云</Typography>
          <Box flexGrow={1}></Box>
          <Button className={classes.userButton} onClick={openUserDrawer}>
            <AccountCircleIcon className={classes.icon} />
            <Typography variant="h6">{app.user.name}</Typography>
          </Button>
        </Toolbar>
      </AppBar>
      <LeftDrawer />
      <RightDrawer open={showUserDrawer} onClose={closeUserDrawer} />
      <Box flexGrow={1} px={{ sm: 1, md: 2 }} py={2}>
        <div className={classes.toolbar}></div>
        {props.children}
      </Box>
    </Container>
  )
}

export default hot(Home)
