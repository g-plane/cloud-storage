import React, { useState, useEffect, useContext } from 'react'
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import TextField from '@material-ui/core/TextField'
import * as fetch from 'utils/fetch'
import { File } from '~/types'
import useSnackbar from '~/shared/useSnackbar'
import AppContext from '~/shared/AppContext'

interface Props {
  open: boolean
  file: File
  onRename(name: string): void
  onClose(): void
}

const RenameItemDialog: React.FC<Props> = (props) => {
  const { open, file, onRename, onClose } = props

  const [name, setName] = useState('')
  const [app] = useContext(AppContext)
  const [Snackbar, setShowSnackbar] = useSnackbar()

  useEffect(() => {
    setName(file.name)
  }, [file])

  const handleNameChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setName(event.target.value)
  }

  const handleRename = async () => {
    if (name === file.name) {
      onClose()
      return
    }

    const { id } = file
    const {
      code,
      message,
    }: { code: number; message: string } = await fetch.put('/files/rename', {
      id,
      name,
      location: app.location.current.id,
    })

    setShowSnackbar(message)
    if (code === 0) {
      onClose()
      onRename(name)
    }
  }

  return (
    <>
      <Dialog open={open} onClose={onClose}>
        <DialogTitle>重命名</DialogTitle>
        <DialogContent>
          <TextField
            label="新名称"
            value={name}
            onChange={handleNameChange}
            fullWidth
          />
        </DialogContent>
        <DialogActions>
          <Button color="primary" onClick={handleRename}>
            确定
          </Button>
          <Button color="primary" onClick={onClose}>
            取消
          </Button>
        </DialogActions>
      </Dialog>
      <Snackbar />
    </>
  )
}

export default RenameItemDialog
