import React from 'react'
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import * as fetch from 'utils/fetch'
import { File } from '~/types'
import useSnackbar from '~/shared/useSnackbar'

interface Props {
  open: boolean
  file: File
  onDelete(file: File, size: number): void
  onClose(): void
}

const DeleteItemDialog: React.FC<Props> = (props) => {
  const { open, file, onDelete, onClose } = props

  const [Snackbar, setShowSnackbar] = useSnackbar()

  const handleDelete = async () => {
    const { id } = file
    const {
      code,
      message,
      size,
    }: {
      code: number
      message: string
      size: number
    } = await fetch.del('/files/rm', { id })

    setShowSnackbar(message)
    if (code === 0) {
      onDelete(file, size)
    }
    onClose()
  }

  return (
    <>
      <Dialog open={open} onClose={onClose}>
        <DialogTitle>删除</DialogTitle>
        <DialogContent>
          <DialogContentText>
            确定要将 {file.name}{' '}
            {file.type === 'directory' && '及其子文件和文件夹'}
            移至回收站吗？
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button color="primary" onClick={handleDelete}>
            确定
          </Button>
          <Button color="primary" onClick={onClose}>
            取消
          </Button>
        </DialogActions>
      </Dialog>
      <Snackbar />
    </>
  )
}

export default DeleteItemDialog
