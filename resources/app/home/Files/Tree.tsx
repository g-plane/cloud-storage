import React, { useState, useEffect } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import grey from '@material-ui/core/colors/grey'
import Box from '@material-ui/core/Box'
import TreeItem from '@material-ui/lab/TreeItem'
import TreeView from '@material-ui/lab/TreeView'
import Typography from '@material-ui/core/Typography'
import ChevronRightIcon from '@material-ui/icons/ChevronRight'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'
import FolderIcon from '@material-ui/icons/Folder'
import * as fetch from 'utils/fetch'

type Node = {
  id: number
  name: string
  directories: Node[]
}

const useStyles = makeStyles(() => ({
  root: {
    '&:focus > $content, &$selected > $content': {
      backgroundColor: grey[400],
    },
    '&:focus > $content $label, &:hover > $content $label, &$selected > $content $label': {
      backgroundColor: 'transparent',
    },
  },
  content: {},
  selected: {},
  icon: {
    marginRight: '5px',
    color: '#777',
  },
  text: {
    flexGrow: 1,
  },
}))

interface Props {
  root: number
  selected: number
  onSelect(id: number): void
}

const Tree: React.FC<Props> = (props) => {
  const { root } = props

  const [tree, setTree] = useState<Node[]>([])
  const classes = useStyles()

  useEffect(() => {
    if (root < 0) {
      setTree([])
      return
    }

    const getTree = async () => {
      const tree: Node[] = await fetch.get('/files/lst', { root })
      setTree(tree)
    }
    getTree()
  }, [root])

  const handleNodeSelect = (_: React.ChangeEvent<{}>, value: string) => {
    props.onSelect(Number.parseInt(value))
  }

  const visit = (node: Node) => (
    <TreeItem
      key={node.id}
      nodeId={node.id.toString()}
      label={
        <Box display="flex" alignItems="center" py={0.5}>
          <FolderIcon className={classes.icon} />
          <Typography component="span" className={classes.text}>
            {node.name}
          </Typography>
        </Box>
      }
      classes={{
        root: classes.root,
        content: classes.content,
        selected: classes.selected,
      }}
    >
      {node.directories.map(visit)}
    </TreeItem>
  )

  return (
    <TreeView
      defaultCollapseIcon={<ExpandMoreIcon />}
      defaultExpandIcon={<ChevronRightIcon />}
      defaultExpanded={[root.toString()]}
      selected={props.selected.toString()}
      onNodeSelect={handleNodeSelect}
    >
      {visit({ id: root, name: '主目录', directories: tree })}
    </TreeView>
  )
}

export default Tree
