import React, { useState, useContext } from 'react'
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import * as fetch from 'utils/fetch'
import { File } from '~/types'
import useSnackbar from '~/shared/useSnackbar'
import AppContext from '~/shared/AppContext'
import Tree from './Tree'

interface Props {
  file: File | null
  onCopy(): void
  onClose(): void
}

const CopyItemDialog: React.FC<Props> = (props) => {
  const { file, onCopy, onClose } = props

  const [destination, setDestination] = useState(-1)
  const [app, setApp] = useContext(AppContext)
  const [Snackbar, setShowSnackbar] = useSnackbar()

  const handleSelect = (id: number) => setDestination(id)

  const handleClose = () => {
    setDestination(-1)
    onClose()
  }

  const handleCopy = async () => {
    if (!file || destination === -1) {
      return
    }

    const { id } = file
    const {
      code,
      message,
    }: { code: number; message: string } = await fetch.post('/files/cp', {
      id,
      destination,
    })

    setShowSnackbar(message)
    if (code === 0) {
      setApp((app) => {
        app.user.used_space += file.size
      })
      onCopy()
    }
    handleClose()
  }

  return (
    <>
      {file && (
        <Dialog open fullWidth maxWidth="sm" onClose={handleClose}>
          <DialogTitle>复制</DialogTitle>
          <DialogContent>
            <DialogContentText>请选择目标文件夹：</DialogContentText>
            <Tree
              root={app.user.storage_id}
              selected={destination}
              onSelect={handleSelect}
            />
          </DialogContent>
          <DialogActions>
            <Button color="primary" onClick={handleCopy}>
              确定
            </Button>
            <Button color="primary" onClick={handleClose}>
              取消
            </Button>
          </DialogActions>
        </Dialog>
      )}
      <Snackbar />
    </>
  )
}

export default CopyItemDialog
