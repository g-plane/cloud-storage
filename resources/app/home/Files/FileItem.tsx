import React, { useState, useEffect } from 'react'
import IconButton from '@material-ui/core/IconButton'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction'
import ListItemText from '@material-ui/core/ListItemText'
import Tooltip from '@material-ui/core/Tooltip'
import Typography from '@material-ui/core/Typography'
import DeleteIcon from '@material-ui/icons/Delete'
import EditIcon from '@material-ui/icons/Edit'
import FileCopyIcon from '@material-ui/icons/FileCopy'
import GetAppIcon from '@material-ui/icons/GetApp'
import InsertDriveFileIcon from '@material-ui/icons/InsertDriveFile'
import ScreenShareIcon from '@material-ui/icons/ScreenShare'
import filesize from 'filesize'
import useStyles from './styles'
import type { File } from '~/types'
import type { Action } from './types'

interface Props {
  file: File
  onCopyClick(): void
  onMoveClick(): void
  onRenameClick(): void
  onDeleteClick(): void
}

const FileItem: React.FC<Props> = (props) => {
  const { file } = props
  const [actions, setActions] = useState<Action[]>([])
  const classes = useStyles()

  useEffect(() => {
    const payload = { actions: [], file }
    events.emit('fileActions', payload)
    setActions(payload.actions)
  }, [])

  const handleClick = () => {
    events.emit('openFile', { file })
  }

  const handleDownloadClick = () => {
    window.open(`/files/download/${file.id}`)
  }

  const createdAt = new Date(file.created_at).toLocaleString()
  const size = filesize(file.size)

  return (
    <ListItem button onClick={handleClick}>
      <ListItemIcon>
        <InsertDriveFileIcon />
      </ListItemIcon>
      <ListItemText secondary={`上传于 ${createdAt}，大小 ${size}`}>
        <Typography>{file.name}</Typography>
      </ListItemText>
      <ListItemSecondaryAction>
        {actions.map((action) => (
          <Tooltip key={action.tooltip} title={action.tooltip}>
            <IconButton
              edge="end"
              className={classes.iconButton}
              onClick={action.handler}
            >
              {action.icon}
            </IconButton>
          </Tooltip>
        ))}
        <Tooltip title="下载文件">
          <IconButton
            edge="end"
            className={classes.iconButton}
            onClick={handleDownloadClick}
          >
            <GetAppIcon />
          </IconButton>
        </Tooltip>
        <Tooltip title="复制">
          <IconButton
            edge="end"
            className={classes.iconButton}
            onClick={props.onCopyClick}
          >
            <FileCopyIcon />
          </IconButton>
        </Tooltip>
        <Tooltip title="移动">
          <IconButton
            edge="end"
            className={classes.iconButton}
            onClick={props.onMoveClick}
          >
            <ScreenShareIcon />
          </IconButton>
        </Tooltip>
        <Tooltip title="重命名">
          <IconButton
            edge="end"
            className={classes.iconButton}
            onClick={props.onRenameClick}
          >
            <EditIcon />
          </IconButton>
        </Tooltip>
        <Tooltip title="删除">
          <IconButton edge="end" onClick={props.onDeleteClick}>
            <DeleteIcon />
          </IconButton>
        </Tooltip>
      </ListItemSecondaryAction>
    </ListItem>
  )
}

export default FileItem
