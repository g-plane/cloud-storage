import React, { useContext, useState, useEffect } from 'react'
import { hot } from 'react-hot-loader/root'
import { useImmer } from 'use-immer'
import Box from '@material-ui/core/Box'
import Breadcrumbs from '@material-ui/core/Breadcrumbs'
import Button from '@material-ui/core/Button'
import CircularProgress from '@material-ui/core/CircularProgress'
import Container from '@material-ui/core/Container'
import Link from '@material-ui/core/Link'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import Typography from '@material-ui/core/Typography'
import AddIcon from '@material-ui/icons/Add'
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward'
import * as fetch from 'utils/fetch'
import { File } from '~/types'
import AppContext from '~/shared/AppContext'
import DirectoryItem from './DirectoryItem'
import FileItem from './FileItem'
import CreateDialog from './CreateDialog'
import CopyItemDialog from './CopyItemDialog'
import MoveItemDialog from './MoveItemDialog'
import RenameItemDialog from './RenameItemDialog'
import DeleteItemDialog from './DeleteItemDialog'
import Upload from '../Upload'

type MenuItem = {
  title: string
  icon: React.ReactNode
  handler(event: React.MouseEvent): void
}

const Files: React.FC = () => {
  const [app, setApp] = useContext(AppContext)
  const [isLoading, setIsLoading] = useState(false)
  const [files, setFiles] = useImmer<File[]>([])
  const [showCreateDialog, setShowCreateDialog] = useState(false)
  const [copying, setCopying] = useState(-1)
  const [moving, setMoving] = useState(-1)
  const [renaming, setRenaming] = useState(-1)
  const [deleting, setDeleting] = useState(-1)
  const [menu, setMenu] = useState<MenuItem[]>([])

  useEffect(() => {
    const getFiles = async () => {
      setIsLoading(true)
      setApp((app) => {
        app.location.ancestors = []
        app.location.current = { id: app.user.storage_id, name: '主目录' }
      })
      const all: File[] = await fetch.get('/files/ls', {
        location: app.user.storage_id,
      })
      setFiles(() => all)
      setIsLoading(false)
    }
    getFiles()
  }, [app.user.storage_id])

  useEffect(() => {
    const menuItems: MenuItem[] = []
    events.emit('filesListMenu', { menuItems })
    setMenu(menuItems)
  }, [])

  const openCreateDialog = () => setShowCreateDialog(true)
  const closeCreateDialog = () => setShowCreateDialog(false)

  const enterDirectory = async (id: number, name: string) => {
    const files: File[] = await fetch.get('/files/ls', { location: id })
    setFiles(() => files)
    setApp((app) => {
      app.location.ancestors.push(app.location.current)
      app.location.current = { id, name }
    })
  }

  const goUp = async (index?: number) => {
    const i = index ?? app.location.ancestors.length - 1
    const target = app.location.ancestors[i]
    const files: File[] = await fetch.get('/files/ls', { location: target.id })
    setFiles(() => files)
    setApp((app) => {
      app.location.ancestors.splice(i, app.location.ancestors.length - i)
      app.location.current = target
    })
  }

  const handleCreateDirectory = (directory: File) => {
    setFiles((files) => {
      files.push(directory)
    })
  }

  const handleUpload = (file: File) => {
    setFiles((files) => {
      files.push(file)
    })
    setApp((app) => {
      app.user.used_space += file.size
    })
  }

  const refresh = async () => {
    const all: File[] = await fetch.get('/files/ls', {
      location: app.location.current.id,
    })
    setFiles(() => all)
  }

  const cancelCopy = () => setCopying(-1)

  const cancelMove = () => setMoving(-1)

  const handleRename = (name: string) => {
    setFiles((files) => {
      files[renaming].name = name
    })
  }

  const cancelRename = () => setRenaming(-1)

  const handleDelete = (item: File, size: number) => {
    setFiles((files) => files.filter((file) => file.id !== item.id))
    setApp((app) => {
      app.user.used_space -= size
    })
  }

  const cancelDelete = () => setDeleting(-1)

  return (
    <>
      <Container maxWidth="xl">
        <div>
          <Button
            startIcon={<AddIcon />}
            color="secondary"
            onClick={openCreateDialog}
          >
            创建文件夹
          </Button>
          <Upload onUpload={handleUpload} />
          {menu.map((item) => (
            <Button
              startIcon={item.icon}
              color="secondary"
              onClick={item.handler}
            >
              {item.title}
            </Button>
          ))}
        </div>
        <Box mt={1}>
          <Breadcrumbs>
            {app.location.ancestors.length > 0 &&
              app.location.ancestors.map((ancestor, i) => (
                <Link href="#" onClick={() => goUp(i)}>
                  {ancestor.name}
                </Link>
              ))}
            <Typography>{app.location.current.name}</Typography>
          </Breadcrumbs>
        </Box>
        {isLoading ? (
          <Box display="flex" justifyContent="center">
            <CircularProgress />
          </Box>
        ) : files.length === 0 &&
          app.location.current.id === app.user.storage_id ? (
          <Typography align="center">存储库是空的</Typography>
        ) : (
          <List>
            {app.location.current.id !== app.user.storage_id && (
              <ListItem button onClick={() => goUp()}>
                <ListItemIcon>
                  <ArrowUpwardIcon />
                </ListItemIcon>
                <ListItemText>返回上一级</ListItemText>
              </ListItem>
            )}
            {files.map((file, index) =>
              file.type === 'directory' ? (
                <DirectoryItem
                  key={file.id}
                  file={file}
                  onClick={enterDirectory}
                  onCopyClick={() => setCopying(index)}
                  onMoveClick={() => setMoving(index)}
                  onRenameClick={() => setRenaming(index)}
                  onDeleteClick={() => setDeleting(index)}
                />
              ) : (
                <FileItem
                  key={file.id}
                  file={file}
                  onCopyClick={() => setCopying(index)}
                  onMoveClick={() => setMoving(index)}
                  onRenameClick={() => setRenaming(index)}
                  onDeleteClick={() => setDeleting(index)}
                />
              ),
            )}
          </List>
        )}
      </Container>
      <CreateDialog
        open={showCreateDialog}
        onClose={closeCreateDialog}
        onAdd={handleCreateDirectory}
      />
      <CopyItemDialog
        file={files[copying]}
        onCopy={refresh}
        onClose={cancelCopy}
      />
      <MoveItemDialog
        file={files[moving]}
        onMove={refresh}
        onClose={cancelMove}
      />
      <RenameItemDialog
        open={renaming > -1}
        file={files[renaming] ?? {}}
        onRename={handleRename}
        onClose={cancelRename}
      />
      <DeleteItemDialog
        open={deleting > -1}
        file={files[deleting] ?? {}}
        onDelete={handleDelete}
        onClose={cancelDelete}
      />
    </>
  )
}

export default hot(Files)
