import React, { useContext, useState } from 'react'
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import TextField from '@material-ui/core/TextField'
import * as fetch from 'utils/fetch'
import { File } from '~/types'
import useSnackbar from '~/shared/useSnackbar'
import AppContext from '~/shared/AppContext'

interface Props {
  open: boolean
  onClose(): void
  onAdd(dir: File): void
}

const CreateDialog: React.FC<Props> = (props) => {
  const { open, onClose } = props

  const [app] = useContext(AppContext)
  const [name, setName] = useState('')
  const [Snackbar, setSnackbar] = useSnackbar()

  const handleNewNameChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setName(event.target.value)
  }

  const handleClose = () => {
    setName('')
    onClose()
  }

  const handleCreateDirectory = async () => {
    const {
      code,
      message,
      dir,
    }: { code: number; message: string; dir: File } = await fetch.post(
      '/files/mkdir',
      {
        name,
        location: app.location.current.id,
      },
    )

    setSnackbar(message)
    if (code === 0) {
      props.onAdd(dir)
      handleClose()
    }
  }

  return (
    <>
      <Dialog open={open} onClose={handleClose}>
        <DialogTitle>创建文件夹</DialogTitle>
        <DialogContent>
          <DialogContentText>
            将在当前位置创建一个新的文件夹。
          </DialogContentText>
          <TextField
            label="文件夹名称"
            value={name}
            onChange={handleNewNameChange}
            fullWidth
          />
        </DialogContent>
        <DialogActions>
          <Button color="primary" onClick={handleCreateDirectory}>
            确定
          </Button>
          <Button color="primary" onClick={handleClose}>
            取消
          </Button>
        </DialogActions>
      </Dialog>
      <Snackbar />
    </>
  )
}

export default CreateDialog
