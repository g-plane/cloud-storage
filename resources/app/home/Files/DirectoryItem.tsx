import React, { useState, useEffect } from 'react'
import IconButton from '@material-ui/core/IconButton'
import Link from '@material-ui/core/Link'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction'
import ListItemText from '@material-ui/core/ListItemText'
import Tooltip from '@material-ui/core/Tooltip'
import DeleteIcon from '@material-ui/icons/Delete'
import EditIcon from '@material-ui/icons/Edit'
import FileCopyIcon from '@material-ui/icons/FileCopy'
import FolderIcon from '@material-ui/icons/Folder'
import ScreenShareIcon from '@material-ui/icons/ScreenShare'
import useStyles from './styles'
import type { File } from '~/types'
import type { Action } from './types'

interface Props {
  file: File
  onClick(id: number, name: string): Promise<void> | void
  onCopyClick(): void
  onMoveClick(): void
  onRenameClick(): void
  onDeleteClick(): void
}

const DirectoryItem: React.FC<Props> = (props) => {
  const { file } = props
  const [actions, setActions] = useState<Action[]>([])
  const classes = useStyles()

  useEffect(() => {
    const payload = { actions: [], file }
    events.emit('fileActions', payload)
    setActions(payload.actions)
  }, [])

  const handleClick = () => props.onClick(file.id, file.name)

  const createdAt = new Date(file.created_at).toLocaleString()

  return (
    <ListItem button onClick={handleClick}>
      <ListItemIcon>
        <FolderIcon />
      </ListItemIcon>
      <ListItemText secondary={`创建于 ${createdAt}`}>
        <Link href="#" onClick={handleClick}>
          {file.name}
        </Link>
      </ListItemText>
      <ListItemSecondaryAction>
        {actions.map((action) => (
          <Tooltip key={action.tooltip} title={action.tooltip}>
            <IconButton
              edge="end"
              className={classes.iconButton}
              onClick={action.handler}
            >
              {action.icon}
            </IconButton>
          </Tooltip>
        ))}
        <Tooltip title="复制">
          <IconButton
            edge="end"
            className={classes.iconButton}
            onClick={props.onCopyClick}
          >
            <FileCopyIcon />
          </IconButton>
        </Tooltip>
        <Tooltip title="移动">
          <IconButton
            edge="end"
            className={classes.iconButton}
            onClick={props.onMoveClick}
          >
            <ScreenShareIcon />
          </IconButton>
        </Tooltip>
        <Tooltip title="重命名">
          <IconButton
            edge="end"
            className={classes.iconButton}
            onClick={props.onRenameClick}
          >
            <EditIcon />
          </IconButton>
        </Tooltip>
        <Tooltip title="删除">
          <IconButton edge="end" onClick={props.onDeleteClick}>
            <DeleteIcon />
          </IconButton>
        </Tooltip>
      </ListItemSecondaryAction>
    </ListItem>
  )
}

export default DirectoryItem
