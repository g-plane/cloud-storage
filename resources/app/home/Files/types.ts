import type { ReactNode } from 'react'

export type Action = {
  icon: ReactNode
  tooltip: string
  handler(event: React.MouseEvent): void
}
