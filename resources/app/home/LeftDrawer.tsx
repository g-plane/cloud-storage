import React, { useContext, useState, useEffect } from 'react'
import { makeStyles, Theme } from '@material-ui/core/styles'
import Box from '@material-ui/core/Box'
import Drawer from '@material-ui/core/Drawer'
import LinearProgress from '@material-ui/core/LinearProgress'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import Typography from '@material-ui/core/Typography'
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline'
import HomeIcon from '@material-ui/icons/Home'
import { useNavigate } from '@reach/router'
import filesize from 'filesize'
import AppContext from '~/shared/AppContext'

type Navigation = {
  link: string
  title: string
  icon: React.ReactElement
}

const useStyles = makeStyles((theme: Theme) => ({
  drawer: {
    width: 250,
    flexShrink: 0,
  },
  drawerPaper: {
    width: 250,
  },
  toolbar: theme.mixins.toolbar,
  content: {
    display: 'flex',
    flexDirection: 'column',
    flexGrow: 1,
    justifyContent: 'space-between',
  },
}))

const LeftDrawer: React.FC = () => {
  const [app] = useContext(AppContext)
  const [navigations, setNavigations] = useState<Navigation[]>([])
  const classes = useStyles()
  const navigate = useNavigate()

  useEffect(() => {
    const navigations = [
      { link: '/home', title: '所有文件', icon: <HomeIcon /> },
      { link: '/home/trash', title: '回收站', icon: <DeleteOutlineIcon /> },
    ]
    events.emit('homeLeftDrawer', { navigations })
    setNavigations(navigations)
  }, [])

  const unused = app.user.total_space - app.user.used_space
  const usedPercentage = (app.user.used_space / app.user.total_space) * 100

  return (
    <Drawer
      className={classes.drawer}
      classes={{ paper: classes.drawerPaper }}
      variant="permanent"
    >
      <div className={classes.toolbar}></div>
      <Box className={classes.content}>
        <List>
          {navigations.map((navigation) => (
            <ListItem
              button
              key={navigation.link}
              onClick={() => navigate(navigation.link)}
            >
              <ListItemIcon>{navigation.icon}</ListItemIcon>
              <ListItemText primary={navigation.title} />
            </ListItem>
          ))}
        </List>
        <Box mb={1}>
          <Box px={2}>
            <Typography variant="subtitle1">存储用量：</Typography>
            <LinearProgress variant="determinate" value={usedPercentage} />
            <Typography variant="subtitle2">
              已用：{filesize(app.user.used_space)}
            </Typography>
            <Typography variant="subtitle2">
              剩余：{filesize(unused)}
            </Typography>
            <Typography variant="subtitle2">
              总容量：{filesize(app.user.total_space)}
            </Typography>
          </Box>
        </Box>
      </Box>
    </Drawer>
  )
}

export default LeftDrawer
