import React, { useState, useContext, useEffect } from 'react'
import { hot } from 'react-hot-loader/root'
import Box from '@material-ui/core/Box'
import Button from '@material-ui/core/Button'
import Container from '@material-ui/core/Container'
import Link from '@material-ui/core/Link'
import TextField from '@material-ui/core/TextField'
import Typography from '@material-ui/core/Typography'
import { makeStyles } from '@material-ui/core/styles'
import CloudCircleIcon from '@material-ui/icons/CloudCircle'
import { Link as RouterLink, useNavigate } from '@reach/router'
import * as fetch from 'utils/fetch'
import { User } from '~/types'
import AppContext from '~/shared/AppContext'
import useSnackbar from '~/shared/useSnackbar'

const useStyles = makeStyles(() => ({
  icon: {
    fontSize: '60px',
  },
}))

const Login: React.FC = () => {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [Snackbar, setShowSnackbar] = useSnackbar()
  const [, setApp] = useContext(AppContext)
  const classes = useStyles()
  const navigate = useNavigate()

  useEffect(() => {
    fetch.get('/sanctum/csrf-cookie')
  }, [])

  const handleEmailChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setEmail(event.target.value)
  }

  const handlePasswordChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setPassword(event.target.value)
  }

  const handleButtonClick = async () => {
    const {
      code,
      message,
      user,
    }: {
      code: number
      message: string
      user: User
    } = await fetch.post('/auth/login', {
      email,
      password,
    })

    if (code === 0) {
      setApp((app) => {
        app.user = user
      })
      navigate('/home')
    } else {
      setShowSnackbar(message)
    }
  }

  return (
    <Container component="main" maxWidth="xs">
      <Box display="flex" flexDirection="column" alignItems="center" mt={5}>
        <CloudCircleIcon className={classes.icon} color="secondary" />
        <Typography variant="h6">登录</Typography>
        <Box mt={2}>
          <Typography variant="subtitle1">
            <small>欢迎使用云存储系统</small>
          </Typography>
        </Box>
        <Box width="100%" mt={3}>
          <form action="post" autoComplete="off">
            <Box mt={2}>
              <TextField
                label="邮箱地址"
                type="email"
                variant="outlined"
                fullWidth
                value={email}
                onChange={handleEmailChange}
              />
            </Box>
            <Box mt={2}>
              <TextField
                label="密码"
                type="password"
                variant="outlined"
                fullWidth
                value={password}
                onChange={handlePasswordChange}
              />
            </Box>
            <Box display="flex" justifyContent="center" mt={6}>
              <Button
                variant="contained"
                color="primary"
                fullWidth
                onClick={handleButtonClick}
              >
                登录
              </Button>
            </Box>
            <Box mt={2}>
              <Link component={RouterLink} to="/auth/register">
                注册新账号
              </Link>
            </Box>
          </form>
        </Box>
      </Box>
      <Snackbar />
    </Container>
  )
}

export default hot(Login)
