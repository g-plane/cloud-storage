import React, { useState, useEffect, useContext } from 'react'
import { hot } from 'react-hot-loader/root'
import Box from '@material-ui/core/Box'
import Button from '@material-ui/core/Button'
import Container from '@material-ui/core/Container'
import Link from '@material-ui/core/Link'
import TextField from '@material-ui/core/TextField'
import Typography from '@material-ui/core/Typography'
import { makeStyles } from '@material-ui/core/styles'
import CloudCircleIcon from '@material-ui/icons/CloudCircle'
import { Link as RouterLink, useNavigate } from '@reach/router'
import * as fetch from 'utils/fetch'
import { User } from '~/types'
import AppContext from '~/shared/AppContext'
import useSnackbar from '~/shared/useSnackbar'

const useStyles = makeStyles(() => ({
  icon: {
    fontSize: '60px',
  },
}))

const Register: React.FC = () => {
  const [name, setName] = useState('')
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [confirm, setConfirm] = useState('')
  const [, setApp] = useContext(AppContext)
  const [Snackbar, setShowSnackbar] = useSnackbar()
  const classes = useStyles()
  const navigate = useNavigate()

  useEffect(() => {
    fetch.get('/sanctum/csrf-cookie')
  }, [])

  const handleUsernameChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setName(event.target.value)
  }

  const handleEmailChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setEmail(event.target.value)
  }

  const handlePasswordChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setPassword(event.target.value)
  }

  const handleConfirmChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setConfirm(event.target.value)
  }

  const handleButtonClick = async () => {
    const {
      code,
      message,
      user,
    }: { code: number; message: string; user: User } = await fetch.post(
      '/auth/register',
      {
        name,
        email,
        password,
        password_confirmation: confirm,
      },
    )
    setShowSnackbar(message)

    if (code === 0) {
      setApp((app) => {
        app.user = user
      })
      setTimeout(() => navigate('/home'), 1500)
    }
  }

  const isConfirmMatched = confirm === '' || password === confirm

  return (
    <Container component="main" maxWidth="xs">
      <Box display="flex" flexDirection="column" alignItems="center" mt={5}>
        <CloudCircleIcon className={classes.icon} color="secondary" />
        <Typography variant="h6">注册</Typography>
        <Box width="100%" mt={3}>
          <form action="post" autoComplete="off">
            <Box mt={2}>
              <TextField
                label="邮箱地址"
                type="email"
                required
                variant="outlined"
                fullWidth
                value={email}
                onChange={handleEmailChange}
              />
            </Box>
            <Box mt={2}>
              <TextField
                label="用户名"
                type="text"
                required
                variant="outlined"
                fullWidth
                value={name}
                onChange={handleUsernameChange}
              />
            </Box>
            <Box mt={2}>
              <TextField
                label="密码"
                type="password"
                required
                variant="outlined"
                fullWidth
                autoComplete="new-password"
                value={password}
                onChange={handlePasswordChange}
              />
            </Box>
            <Box mt={2}>
              <TextField
                label="再输入一次密码"
                type="password"
                required
                variant="outlined"
                fullWidth
                error={!isConfirmMatched}
                helperText={isConfirmMatched ? ' ' : '确认密码不正确'}
                value={confirm}
                onChange={handleConfirmChange}
              />
            </Box>
            <Box display="flex" justifyContent="center" mt={4}>
              <Button
                variant="contained"
                color="primary"
                fullWidth
                onClick={handleButtonClick}
              >
                注册
              </Button>
            </Box>
            <Box mt={2}>
              <Link component={RouterLink} to="/auth/login">
                已有账号？直接登录
              </Link>
            </Box>
          </form>
        </Box>
      </Box>
      <Snackbar />
    </Container>
  )
}

export default hot(Register)
