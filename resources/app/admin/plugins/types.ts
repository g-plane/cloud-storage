export type Plugin = {
  id: number
  name: string
  title: string
  version: string
  is_enabled: boolean
}
