import React from 'react'
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import * as fetch from 'utils/fetch'
import { Plugin } from './types'

interface Props {
  plugin: Plugin | null
  onDelete(plugin: Plugin, message: string): void
  onClose(): void
}

const DeletePluginDialog: React.FC<Props> = (props) => {
  const { plugin, onDelete, onClose } = props

  const handleDelete = async () => {
    if (!plugin) {
      return
    }

    const {
      code,
      message,
    }: { code: number; message: string } = await fetch.del(
      `/admin/plugins/${plugin.name}`,
    )

    if (code === 0) {
      onDelete(plugin, message)
    }
    onClose()
  }

  if (!plugin) {
    return null
  }

  return (
    <Dialog open onClose={onClose}>
      <DialogTitle>删除</DialogTitle>
      <DialogContent>
        <DialogContentText>
          确定要删除 {plugin.title} 插件吗？此操作不可撤销。
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button color="primary" onClick={handleDelete}>
          确定
        </Button>
        <Button color="primary" onClick={onClose}>
          取消
        </Button>
      </DialogActions>
    </Dialog>
  )
}

export default DeletePluginDialog
