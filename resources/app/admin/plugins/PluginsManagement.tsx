import React, { useState, useEffect } from 'react'
import { hot } from 'react-hot-loader/root'
import { useImmer } from 'use-immer'
import Box from '@material-ui/core/Box'
import Button from '@material-ui/core/Button'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import CircularProgress from '@material-ui/core/CircularProgress'
import Container from '@material-ui/core/Container'
import Divider from '@material-ui/core/Divider'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction'
import ListItemText from '@material-ui/core/ListItemText'
import Typography from '@material-ui/core/Typography'
import * as fetch from 'utils/fetch'
import useSnackbar from '~/shared/useSnackbar'
import { Plugin } from './types'
import DeletePluginDialog from './DeletePluginDialog'

const PluginsManagement: React.FC = () => {
  const [plugins, setPlugins] = useImmer<Plugin[]>([])
  const [isLoading, setIsLoading] = useState(false)
  const [Snackbar, setShowSnackbar] = useSnackbar()
  const [deleting, setDeleting] = useState<Plugin | null>(null)

  useEffect(() => {
    const getPlugins = async () => {
      setIsLoading(true)
      const plugins: Plugin[] = await fetch.get('/admin/plugins/list')
      setPlugins(() => plugins)
      setIsLoading(false)
    }
    getPlugins()
  }, [])

  const handleTogglePlugin = async (
    { name, is_enabled }: Plugin,
    index: number,
  ) => {
    if (is_enabled) {
      const {
        code,
        message,
      }: { code: number; message: string } = await fetch.put(
        `/admin/plugins/${name}/disable`,
      )
      setShowSnackbar(message)
      if (code !== 0) {
        return
      }
    } else {
      const {
        code,
        message,
      }: { code: number; message: string } = await fetch.put(
        `/admin/plugins/${name}/enable`,
      )
      setShowSnackbar(message)
      if (code !== 0) {
        return
      }
    }

    setPlugins((plugins) => {
      plugins[index].is_enabled = !plugins[index].is_enabled
    })
  }

  const handleDelete = ({ name }: Plugin, message: string) => {
    setShowSnackbar(message)
    setPlugins((plugins) => plugins.filter((plugin) => plugin.name !== name))
  }

  const handleCloseDeleteDialog = () => setDeleting(null)

  return (
    <>
      <Container maxWidth="sm">
        {isLoading ? (
          <Box display="flex" justifyContent="center">
            <CircularProgress />
          </Box>
        ) : plugins.length === 0 ? (
          <Typography align="center">未安装任何插件</Typography>
        ) : (
          <Card>
            <CardContent>
              <Typography variant="h6">插件管理</Typography>
              <List>
                {plugins.map((plugin, i) => (
                  <React.Fragment key={plugin.id}>
                    <ListItem>
                      <ListItemText secondary={`插件标识符：${plugin.name}`}>
                        <Typography>{plugin.title}</Typography>
                      </ListItemText>
                      <ListItemSecondaryAction>
                        <Button
                          color="secondary"
                          onClick={() => handleTogglePlugin(plugin, i)}
                        >
                          {plugin.is_enabled ? '关闭插件' : '开启插件'}
                        </Button>
                        <Button
                          color="secondary"
                          onClick={() => setDeleting(plugin)}
                        >
                          删除插件
                        </Button>
                      </ListItemSecondaryAction>
                    </ListItem>
                    <Divider component="li" />
                  </React.Fragment>
                ))}
              </List>
            </CardContent>
          </Card>
        )}
      </Container>
      <DeletePluginDialog
        plugin={deleting}
        onDelete={handleDelete}
        onClose={handleCloseDeleteDialog}
      />
      <Snackbar />
    </>
  )
}

export default hot(PluginsManagement)
