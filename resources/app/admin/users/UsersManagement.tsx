import React, { useState, forwardRef } from 'react'
import { hot } from 'react-hot-loader/root'
import Container from '@material-ui/core/Container'
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward'
import CheckIcon from '@material-ui/icons/Check'
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft'
import ChevronRightIcon from '@material-ui/icons/ChevronRight'
import ClearIcon from '@material-ui/icons/Clear'
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline'
import EditIcon from '@material-ui/icons/Edit'
import FilterListIcon from '@material-ui/icons/FilterList'
import FirstPageIcon from '@material-ui/icons/FirstPage'
import LastPageIcon from '@material-ui/icons/LastPage'
import RemoveIcon from '@material-ui/icons/Remove'
import SaveAltIcon from '@material-ui/icons/SaveAlt'
import SearchIcon from '@material-ui/icons/Search'
import ViewColumnIcon from '@material-ui/icons/ViewColumn'
import VpnKeyIcon from '@material-ui/icons/VpnKey'
import MaterialTable, { Localization, Column, Query } from 'material-table'
import filesize from 'filesize'
import * as fetch from 'utils/fetch'
import useSnackbar from '~/shared/useSnackbar'
import { User, Pagination } from '~/types'
import PasswordDialog from './PasswordDialog'

const localization: Localization = {
  body: {
    deleteTooltip: '删除',
    editTooltip: '编辑',
    editRow: {
      deleteText: '确定要删除吗？',
      cancelTooltip: '取消',
      saveTooltip: '确定',
    },
  },
  header: {
    actions: '操作',
  },
  pagination: {
    labelDisplayedRows: '第 {from} 到 {to} 条，共 {count} 条',
    labelRowsSelect: '条记录',
    labelRowsPerPage: '每页的数量：',
    firstAriaLabel: '第一页',
    firstTooltip: '第一页',
    previousAriaLabel: '上一页',
    previousTooltip: '上一页',
    nextAriaLabel: '下一页',
    nextTooltip: '下一页',
    lastAriaLabel: '最后一页',
    lastTooltip: '最后一页',
  },
  toolbar: {
    searchPlaceholder: '搜索',
    searchTooltip: '搜索',
  },
}

const columns: Column<User>[] = [
  { title: '邮箱地址', field: 'email' },
  { title: '用户名', field: 'name' },
  {
    title: '已用空间',
    field: 'used_space',
    editable: 'never',
    searchable: false,
    render: (user) => filesize(user.used_space),
  },
  {
    title: '总空间',
    field: 'total_space',
    searchable: false,
    render: (user) => filesize(user.total_space),
  },
  {
    title: '权限角色',
    field: 'role',
    editable: 'never',
    searchable: false,
    render: (user) => (user.role === 'admin' ? '管理员' : '普通用户'),
  },
  {
    title: '注册时间',
    field: 'created_at',
    editable: 'never',
    type: 'datetime',
    searchable: false,
    sorting: false,
  },
]

const tableIcons = {
  Check: forwardRef<SVGSVGElement>((props, ref) => (
    <CheckIcon {...props} ref={ref} />
  )),
  Clear: forwardRef<SVGSVGElement>((props, ref) => (
    <ClearIcon {...props} ref={ref} />
  )),
  Delete: forwardRef<SVGSVGElement>((props, ref) => (
    <DeleteOutlineIcon {...props} ref={ref} />
  )),
  DetailPanel: forwardRef<SVGSVGElement>((props, ref) => (
    <ChevronRightIcon {...props} ref={ref} />
  )),
  Edit: forwardRef<SVGSVGElement>((props, ref) => (
    <EditIcon {...props} ref={ref} />
  )),
  Export: forwardRef<SVGSVGElement>((props, ref) => (
    <SaveAltIcon {...props} ref={ref} />
  )),
  Filter: forwardRef<SVGSVGElement>((props, ref) => (
    <FilterListIcon {...props} ref={ref} />
  )),
  FirstPage: forwardRef<SVGSVGElement>((props, ref) => (
    <FirstPageIcon {...props} ref={ref} />
  )),
  LastPage: forwardRef<SVGSVGElement>((props, ref) => (
    <LastPageIcon {...props} ref={ref} />
  )),
  NextPage: forwardRef<SVGSVGElement>((props, ref) => (
    <ChevronRightIcon {...props} ref={ref} />
  )),
  PreviousPage: forwardRef<SVGSVGElement>((props, ref) => (
    <ChevronLeftIcon {...props} ref={ref} />
  )),
  ResetSearch: forwardRef<SVGSVGElement>((props, ref) => (
    <ClearIcon {...props} ref={ref} />
  )),
  Search: forwardRef<SVGSVGElement>((props, ref) => (
    <SearchIcon {...props} ref={ref} />
  )),
  SortArrow: forwardRef<SVGSVGElement>((props, ref) => (
    <ArrowDownwardIcon {...props} ref={ref} />
  )),
  ThirdStateCheck: forwardRef<SVGSVGElement>((props, ref) => (
    <RemoveIcon {...props} ref={ref} />
  )),
  ViewColumn: forwardRef<SVGSVGElement>((props, ref) => (
    <ViewColumnIcon {...props} ref={ref} />
  )),
}

const UsersManagement: React.FC = () => {
  const [isLoading, setIsLoading] = useState(false)
  const [updatingPassword, setUpdatingPassword] = useState<User | null>(null)
  const [Snackbar, setShowSnackbar] = useSnackbar()

  const handleQueryChange = async (query: Query<User>) => {
    setIsLoading(true)
    const result: Pagination<User> = await fetch.get('/admin/users/list', {
      search: query.search,
      page: query.page + 1,
      orderBy: query.orderBy?.field ?? 'id',
      orderDirection: query.orderDirection || 'asc',
    })
    setIsLoading(false)

    return {
      data: result.data,
      page: result.current_page - 1,
      totalCount: result.total,
    }
  }

  const handleUpdate = async (user: User) => {
    const {
      code,
      message,
    }: { code: number; message: string } = await fetch.put(
      `/admin/users/${user.id}`,
      {
        email: user.email,
        name: user.name,
        total_space: user.total_space,
      },
    )

    if (code !== 0) {
      setShowSnackbar(message)
    }
  }

  const handlePasswordChange = async (user: User) => {
    setUpdatingPassword(user)
  }

  const handlePasswordDialogClose = () => setUpdatingPassword(null)

  const handleUpdatePassword = () => setShowSnackbar('密码修改成功')

  const handleDelete = async (user: User) => {
    const {
      code,
      message,
    }: { code: number; message: string } = await fetch.del(
      `/admin/users/${user.id}`,
    )

    if (code !== 0) {
      setShowSnackbar(message)
    }
  }

  return (
    <>
      <Container maxWidth="xl">
        <MaterialTable
          columns={columns}
          data={handleQueryChange}
          title="用户管理"
          isLoading={isLoading}
          icons={tableIcons}
          editable={{
            isDeletable: (user) => user.role === 'normal',
            onRowUpdate: handleUpdate,
            onRowDelete: handleDelete,
          }}
          actions={[
            {
              icon: () => <VpnKeyIcon />,
              tooltip: '修改密码',
              onClick: (_, data) => {
                if (Array.isArray(data)) {
                  return
                }
                handlePasswordChange(data)
              },
            },
          ]}
          options={{
            actionsColumnIndex: -1,
            draggable: false,
            pageSize: 10,
            pageSizeOptions: [10],
          }}
          localization={localization}
        ></MaterialTable>
      </Container>
      <PasswordDialog
        user={updatingPassword}
        onUpdate={handleUpdatePassword}
        onClose={handlePasswordDialogClose}
      />
      <Snackbar />
    </>
  )
}

export default hot(UsersManagement)
