import React, { useState } from 'react'
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import TextField from '@material-ui/core/TextField'
import * as fetch from 'utils/fetch'
import useSnackbar from '~/shared/useSnackbar'
import { User } from '~/types'

interface Props {
  user: User | null
  onUpdate(): void
  onClose(): void
}

const PasswordDialog: React.FC<Props> = (props) => {
  const { user, onClose } = props

  const [password, setPassword] = useState('')
  const [Snackbar, setShowSnackbar] = useSnackbar()

  const handlePasswordChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setPassword(event.target.value)
  }

  const handleClose = () => {
    setPassword('')
    onClose()
  }

  const handleSubmit = async () => {
    if (!user) {
      return
    }

    const {
      code,
      message,
    }: {
      code: number
      message: string
    } = await fetch.put(`/admin/users/password/${user.id}`, { password })

    setShowSnackbar(message)
    if (code === 0) {
      props.onUpdate()
      handleClose()
    }
  }

  if (!user) {
    return null
  }

  return (
    <>
      {user && (
        <Dialog open onClose={handleClose}>
          <DialogTitle>修改密码</DialogTitle>
          <DialogContent>
            <DialogContentText>
              您正在修改用户 {user.email} 的密码：
            </DialogContentText>
            <TextField
              label="新密码"
              type="password"
              value={password}
              onChange={handlePasswordChange}
              fullWidth
            />
          </DialogContent>
          <DialogActions>
            <Button color="primary" onClick={handleSubmit}>
              确定
            </Button>
            <Button color="primary" onClick={handleClose}>
              取消
            </Button>
          </DialogActions>
        </Dialog>
      )}
      <Snackbar />
    </>
  )
}

export default PasswordDialog
