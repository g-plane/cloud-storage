import React from 'react'
import { hot } from 'react-hot-loader/root'
import {
  makeStyles,
  createMuiTheme,
  ThemeProvider,
} from '@material-ui/core/styles'
import blue from '@material-ui/core/colors/blue'
import AppBar from '@material-ui/core/AppBar'
import Box from '@material-ui/core/Box'
import Container from '@material-ui/core/Container'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import DeviceHubIcon from '@material-ui/icons/DeviceHub'
import TopNavigation from './TopNavigation'
import { RouteComponentProps } from '@reach/router'

const theme = createMuiTheme({
  palette: {
    primary: blue,
  },
})

const useStyles = makeStyles(() => ({
  icon: {
    marginRight: '5px',
  },
}))

const AdminBase: React.FC<RouteComponentProps> = (props) => {
  const classes = useStyles()

  return (
    <ThemeProvider theme={theme}>
      <Container maxWidth="xl" disableGutters>
        <AppBar position="sticky">
          <Toolbar>
            <DeviceHubIcon className={classes.icon} />
            <Typography variant="h6">系统管理</Typography>
            <Box flexGrow={1} />
            <TopNavigation />
          </Toolbar>
        </AppBar>
        <Box flexGrow={1} px={{ sm: 1, md: 2 }} py={2}>
          {props.children}
        </Box>
      </Container>
    </ThemeProvider>
  )
}

export default hot(AdminBase)
