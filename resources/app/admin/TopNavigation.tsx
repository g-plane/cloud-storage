import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Button from '@material-ui/core/Button'
import ExitToAppIcon from '@material-ui/icons/ExitToApp'
import ExtensionIcon from '@material-ui/icons/Extension'
import PeopleIcon from '@material-ui/icons/People'
import { useNavigate } from '@reach/router'

const useStyles = makeStyles(() => ({
  btn: {
    marginRight: '15px',
    color: '#fff',
  },
}))

const TopNavigation: React.FC = () => {
  const classes = useStyles()
  const navigate = useNavigate()

  const goToUsers = () => {
    navigate('/admin/users')
  }

  const goToPlugins = () => {
    navigate('/admin/plugins')
  }

  const handleBack = () => {
    navigate('/home')
  }

  return (
    <>
      <Button
        className={classes.btn}
        startIcon={<PeopleIcon />}
        onClick={goToUsers}
      >
        用户管理
      </Button>
      <Button
        className={classes.btn}
        startIcon={<ExtensionIcon />}
        onClick={goToPlugins}
      >
        插件管理
      </Button>
      <Button
        className={classes.btn}
        startIcon={<ExitToAppIcon />}
        onClick={handleBack}
      >
        返回「我的云」
      </Button>
    </>
  )
}

export default TopNavigation
