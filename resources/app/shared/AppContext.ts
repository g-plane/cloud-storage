import { createContext } from 'react'
import { User } from '~/types'

type App = {
  user: User
  location: {
    current: { id: number; name: string }
    ancestors: { id: number; name: string }[]
  }
}

type Context = [App, (f: (draft: App) => void | App) => void]

export const defaultAppState: App = {
  user: {
    id: 0,
    name: '',
    email: '',
    used_space: 0,
    total_space: 1,
    role: 'normal',
    storage_id: 0,
  },
  location: {
    current: { id: 0, name: '主目录' },
    ancestors: [],
  },
}

const context = createContext<Context>([defaultAppState, () => {}])
context.displayName = 'AppContext'

export default context
