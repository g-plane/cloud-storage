import React, { useState } from 'react'
import Snackbar, { SnackbarProps } from '@material-ui/core/Snackbar'

type Props = Omit<SnackbarProps, 'open' | 'message'>

export default function useSnackbar(): [
  React.FC<Props>,
  (message: string) => void,
] {
  const [message, setMessage] = useState('')
  const [open, setOpen] = useState(false)

  const handleClose = () => setOpen(false)

  const setShow = (msg: string) => {
    setMessage(msg)
    setOpen(true)
  }

  const Component: React.FC<Props> = (props) => (
    <Snackbar
      autoHideDuration={3000}
      anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
      {...props}
      message={message}
      open={open}
      onClose={handleClose}
    />
  )

  return [Component, setShow]
}
