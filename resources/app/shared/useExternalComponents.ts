import { useState, useEffect, ReactNodeArray } from 'react'
import AppContext from './AppContext'

export default function useExternalComponents(
  event: string,
  extraContext?: object,
) {
  const [external, setExternal] = useState<ReactNodeArray>([])

  useEffect(() => {
    const payload = { components: [], app: AppContext, ...extraContext }
    events.emit(event, payload)
    setExternal(payload.components)
  }, [event, extraContext])

  return external
}
