import React, { useContext, useState, useEffect } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Drawer from '@material-ui/core/Drawer'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import ContactMailIcon from '@material-ui/icons/ContactMail'
import DeviceHubIcon from '@material-ui/icons/DeviceHub'
import ExitToAppIcon from '@material-ui/icons/ExitToApp'
import { useNavigate } from '@reach/router'
import * as fetch from 'utils/fetch'
import AppContext from './AppContext'

type Navigation = {
  title: string
  icon: React.ReactElement
  handler(event: React.MouseEvent): void
}

const useStyles = makeStyles(() => ({
  list: {
    width: '300px',
  },
}))

interface Props {
  open: boolean
  onClose(): void
}

const RightDrawer: React.FC<Props> = (props) => {
  const [app] = useContext(AppContext)
  const [navigations, setNavigations] = useState<Navigation[]>([])
  const classes = useStyles()
  const navigate = useNavigate()

  useEffect(() => {
    const navigations: Navigation[] = []
    events.emit('homeRightDrawer', { navigations })
    setNavigations(navigations)
  }, [])

  const handleLogout = async () => {
    await fetch.post('/auth/logout')
    props.onClose()
    navigate('/auth/login')
  }

  const handleEditProfile = () => {
    navigate('/home/profile')
    props.onClose()
  }

  const handleEnterAdmin = () => {
    navigate('/admin/users')
    props.onClose()
  }

  return (
    <Drawer open={props.open} anchor="right" onClose={props.onClose}>
      <List className={classes.list}>
        <ListItem button onClick={handleLogout}>
          <ListItemIcon>
            <ExitToAppIcon />
          </ListItemIcon>
          <ListItemText primary="登出账号" />
        </ListItem>
        <ListItem button onClick={handleEditProfile}>
          <ListItemIcon>
            <ContactMailIcon />
          </ListItemIcon>
          <ListItemText primary="修改账户资料" />
        </ListItem>
        {navigations.map((navigation) => (
          <ListItem button key={navigation.title} onClick={navigation.handler}>
            <ListItemIcon>{navigation.icon}</ListItemIcon>
            <ListItemText primary={navigation.title} />
          </ListItem>
        ))}
        {app.user.role === 'admin' && (
          <ListItem button onClick={handleEnterAdmin}>
            <ListItemIcon>
              <DeviceHubIcon />
            </ListItemIcon>
            <ListItemText primary="后台管理" />
          </ListItem>
        )}
      </List>
    </Drawer>
  )
}

export default RightDrawer
