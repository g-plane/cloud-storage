export type UserRole = 'normal' | 'admin'

export type User = {
  id: number
  email: string
  name: string
  used_space: number
  total_space: number
  role: UserRole
  storage_id: number
}

export type File = {
  id: number
  name: string
  type: string
  parent: number
  path: string
  sha256: string
  size: number
  user_id: number
  created_at: string
  deleted_at: string | null
}

export type Pagination<T> = {
  data: T[]
  current_page: number
  last_page: number
  from: number
  to: number
  total: number
}
