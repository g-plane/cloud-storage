declare let events: {
  on(eventName: string, listener: Function): void
  emit(eventName: string, payload: object): void
}
