<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::redirect('', 'auth/login');

Route::prefix('auth')->middleware(['guest'])->group(function () {
    Route::get('login', 'SpaController@app');

    Route::get('register', 'SpaController@app');
});

Route::middleware(['auth'])->group(function () {
    Route::prefix('home')->group(function () {
        Route::get('', 'SpaController@app');
        Route::get('user', 'SpaController@user');
        Route::get('trash', 'SpaController@user');
        Route::get('profile', 'SpaController@app');
    });
});

Route::middleware(['auth', 'role:admin'])->prefix('admin')->group(function () {
    Route::get('users', 'SpaController@app');
    Route::get('plugins', 'SpaController@app');
});

Route::fallback('SpaController@app');
