<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('auth')->middleware(['guest'])->group(function () {
    Route::post('login', 'Auth\LoginController@login');

    Route::post('register', 'Auth\RegisterController@register');
});

Route::middleware(['auth'])->group(function () {
    Route::prefix('auth')->group(function () {
        Route::post('logout', 'Auth\LoginController@logout');
    });

    Route::prefix('files')->group(function () {
        Route::get('ls', 'DirectoryController@ls');
        Route::get('lst', 'DirectoryController@lst');
        Route::post('mkdir', 'DirectoryController@mkdir');
        Route::put('rename', 'FileController@rename');
        Route::post('upload', 'FileController@upload');
        Route::get('download/{file}', 'FileController@download');
        Route::delete('rm', 'FileController@rm');
        Route::post('cp', 'FileController@cp');
        Route::put('mv', 'FileController@mv');
    });

    Route::prefix('trash')->group(function () {
        Route::get('list', 'TrashController@list');
        Route::delete('restore', 'TrashController@restore');
        Route::delete('force-delete', 'TrashController@forceDelete');
        Route::delete('clear', 'TrashController@clearAll');
    });

    Route::prefix('profile')->group(function () {
        Route::put('email', 'ProfileController@email');
        Route::put('name', 'ProfileController@name');
        Route::put('password', 'ProfileController@password');
    });
});

Route::middleware(['auth', 'role:admin'])->prefix('admin')->group(function () {
    Route::prefix('users')->group(function () {
        Route::get('list', 'UsersManagementController@users');
        Route::put('{user}', 'UsersManagementController@update');
        Route::put('password/{user}', 'UsersManagementController@password');
        Route::delete('{user}', 'UsersManagementController@remove');
    });

    Route::prefix('plugins')->group(function () {
        Route::get('list', 'PluginsManagementController@list');
        Route::put('{name}/enable', 'PluginsManagementController@enable');
        Route::put('{name}/disable', 'PluginsManagementController@disable');
        Route::delete('{name}', 'PluginsManagementController@delete');
    });
});
