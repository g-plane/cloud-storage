<?php

namespace App\Providers;

use App\Services\PluginManager;
use DB;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton(PluginManager::class);
    }

    public function boot(PluginManager $pluginManager)
    {
        try {
            DB::getPdo();
            $pluginManager->boot();
        } catch (\Exception $e) {
            //
        }
    }
}
