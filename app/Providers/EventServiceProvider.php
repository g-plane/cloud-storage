<?php

namespace App\Providers;

use App\Listeners;
use Illuminate\Auth\Events\Authenticated;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    protected $listen = [
        Authenticated::class => [
            Listeners\DeleteTrashOverTenDays::class,
        ],
        'trash.list' => [
            Listeners\DeleteTrashOverTenDays::class,
        ],
        'plugin.updated' => [
            Listeners\CopyPluginAssets::class,
        ],
    ];
}
