<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;

class AdminPasswordCommand extends Command
{
    protected $signature = 'admin:password {user} {password}';

    protected $description = "Reset an administrator's password.";

    public function handle()
    {
        $u = $this->argument('user');
        $isEmail = (bool) filter_var($u, FILTER_VALIDATE_EMAIL);

        /** @var User */
        $user = $isEmail
            ? User::where('email', $u)->firstOrFail()
            : User::findOrFail($u);

        $user->password = Hash::make($this->argument('password'));
        $user->save();

        $this->info('Password resetted.');
    }
}
