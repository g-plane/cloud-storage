<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;

class AdminSetCommand extends Command
{
    protected $signature = 'admin:set {user}';

    protected $description = 'Set a user as administrator.';

    public function handle()
    {
        $u = $this->argument('user');
        $isEmail = (bool) filter_var($u, FILTER_VALIDATE_EMAIL);

        /** @var User */
        $user = $isEmail
            ? User::where('email', $u)->firstOrFail()
            : User::findOrFail($u);

        $user->role = 'admin';
        $user->save();

        $this->info('Done.');
    }
}
