<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;

class AdminUnsetCommand extends Command
{
    protected $signature = 'admin:unset {user}';

    protected $description = 'Set an administrator as normal user.';

    public function handle()
    {
        $u = $this->argument('user');
        $isEmail = (bool) filter_var($u, FILTER_VALIDATE_EMAIL);

        /** @var User */
        $user = $isEmail
            ? User::where('email', $u)->firstOrFail()
            : User::findOrFail($u);

        $user->role = 'normal';
        $user->save();

        $this->info('Done.');
    }
}
