<?php

namespace App\Services;

use App\Models\Plugin;
use Composer\Autoload\ClassLoader;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

class PluginManager
{
    protected $booted = false;

    /** @var Application */
    protected $app;

    /** @var Dispatcher */
    protected $dispatcher;

    /** @var Filesystem */
    protected $filesystem;

    /** @var ClassLoader */
    protected $loader;

    /** @var Collection */
    protected $manifests;

    /** @var EloquentCollection */
    protected $installed;

    public function __construct(
        Application $app,
        Dispatcher $dispatcher,
        Filesystem $filesystem
    ) {
        $this->app = $app;
        $this->dispatcher = $dispatcher;
        $this->filesystem = $filesystem;
        $this->manifests = new Collection();
        $this->installed = new EloquentCollection();
        $this->loader = new ClassLoader();
    }

    public function all(): EloquentCollection
    {
        if (filled($this->installed)) {
            return $this->installed;
        }

        $this->installed = Plugin::all();

        collect($this->filesystem->directories(base_path('plugins')))
            ->filter(function ($directory) {
                return $this->filesystem->exists($directory.DIRECTORY_SEPARATOR.'composer.json');
            })
            ->each(function ($directory) {
                $manifest = json_decode(
                    $this->filesystem->get($directory.DIRECTORY_SEPARATOR.'composer.json'),
                    true
                );

                $name = Arr::get($manifest, 'name');
                if (empty($name) || str_replace(base_path('plugins').'/', '', $directory) !== $name) {
                    return;
                }

                if ($this->installed->contains('name', $name)) {
                    /** @var Plugin */
                    $plugin = $this->installed->firstWhere('name', $name);
                    $version = Arr::get($manifest, 'version', '');
                    if ($plugin->version !== $version) {
                        $plugin->version = $version;
                        $plugin->save();
                        $this->dispatcher->dispatch('plugin.updated', [$plugin, $manifest]);
                    }
                } else {
                    $plugin = new Plugin();
                    $plugin->name = $name;
                    $plugin->version = Arr::get($manifest, 'version', '');
                    $plugin->is_enabled = false;
                    $plugin->save();
                    $this->installed->push($plugin);
                }

                $this->manifests->put($name, $manifest);
            });

        return $this->installed;
    }

    public function boot()
    {
        if ($this->booted) {
            return;
        }

        $this->all()->each(function (Plugin $plugin) {
            $name = $plugin->name;
            $this->registerPlugin($this->manifests->get($name));
        });
        $this->loader->register();
        $this->getEnabledPlugins()->each(function (Plugin $plugin) {
            $name = $plugin->name;
            $class = Arr::get($this->manifests->get($name), 'extra.plugin.entry');
            $this->app->call($class.'@boot');
        });

        $this->booted = true;
    }

    protected function registerPlugin($manifest)
    {
        $this->loader->addPsr4(
            Str::finish(Arr::get($manifest, 'extra.plugin.namespace'), '\\'),
            base_path('plugins/'.$manifest['name'].'/src')
        );

        $path = base_path('plugins/'.$manifest['name'].'/vendor/autoload.php');
        if ($this->filesystem->exists($path)) {
            $this->filesystem->getRequire($path);
        }
    }

    /**
     * @return Plugin|null
     */
    public function get(string $name)
    {
        return $this->all()->firstWhere('name', $name);
    }

    /**
     * @return array|null
     */
    public function getManifest(string $name)
    {
        return $this->manifests->get($name);
    }

    public function enable($plugin)
    {
        $plugin = $this->get($plugin);
        if ($plugin && !$plugin->is_enabled) {
            $plugin->is_enabled = true;
            $plugin->save();

            $class = Arr::get($this->manifests->get($plugin->name), 'extra.plugin.entry');
            $instance = $this->app->make($class);
            if (method_exists($instance, 'enable')) {
                $this->app->call([$instance, 'enable']);
            }

            $this->dispatcher->dispatch('plugin.enabled', [$plugin]);
        }
    }

    public function disable($plugin)
    {
        $plugin = $this->get($plugin);
        if ($plugin && $plugin->is_enabled) {
            $plugin->is_enabled = false;
            $plugin->save();

            $class = Arr::get($this->manifests->get($plugin->name), 'extra.plugin.entry');
            $instance = $this->app->make($class);
            if (method_exists($instance, 'disable')) {
                $this->app->call([$instance, 'disable']);
            }

            $this->dispatcher->dispatch('plugin.disabled', [$plugin]);
        }
    }

    public function delete($plugin)
    {
        $plugin = $this->get($plugin);
        if ($plugin) {
            $this->disable($plugin->name);

            $class = Arr::get($this->manifests->get($plugin->name), 'extra.plugin.entry');
            $instance = $this->app->make($class);
            if (method_exists($instance, 'delete')) {
                $this->app->call([$instance, 'delete']);
            }

            $this->dispatcher->dispatch('plugin.deleting', [$plugin]);
            $this->filesystem->deleteDirectory(base_path('plugins/'.$plugin->name));
            $this->manifests->pull($plugin->name);
            $plugin->delete();
        }
    }

    protected function getEnabledPlugins(): Collection
    {
        return $this->all()->filter(function (Plugin $plugin) {
            return $plugin->is_enabled;
        });
    }
}
