<?php

namespace App\Services;

use App\Models\File;
use Illuminate\Support\Facades\Storage;

class FileUtils
{
    public static function forceDeleteRecursive(File $item)
    {
        if ($item->type === 'directory') {
            $item->children->each(function (File $child) {
                FileUtils::forceDeleteRecursive($child);
            });
        } else {
            if (File::withTrashed()->where('sha256', $item->sha256)->count() === 1) {
                Storage::delete($item->path);
            }
        }
        $item->forceDelete();
    }
}
