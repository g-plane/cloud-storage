<?php

namespace App\Http\Controllers;

use App\Models\File;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class UsersManagementController extends Controller
{
    public function users(Request $request)
    {
        $search = $request->input('search');

        $query = User::where(function ($query) use ($search) {
            if ($search) {
                $query->where('email', 'like', "%$search%")
                    ->orWhere('name', 'like', "%$search%");
            }
        });

        if ($request->has('orderBy')) {
            $query = $query->orderBy(
                $request->input('orderBy'),
                $request->input('orderDirection')
            );
        }

        return $query->paginate(10);
    }

    public function update(User $user, Request $request)
    {
        $data = $request->validate([
            'email' => ['required', 'email', Rule::unique('users')->ignore($user)],
            'name' => 'required|string|max:255',
            'total_space' => 'required|integer',
        ]);

        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->total_space = (int) $data['total_space'];
        $user->save();

        return response()->json(['code' => 0]);
    }

    public function password(User $user, Request $request)
    {
        $data = $request->validate([
            'password' => 'required|string|min:8|max:32',
        ]);

        $user->password = Hash::make($data['password']);
        $user->save();

        return response()->json(['code' => 0]);
    }

    public function remove(User $user)
    {
        if ($user->role === 'admin') {
            return response()->json(['code' => 1, 'message' => '管理员不能被删除']);
        }

        File::where('user_id', $user->id)->delete();
        $user->delete();

        return response()->json(['code' => 0]);
    }
}
