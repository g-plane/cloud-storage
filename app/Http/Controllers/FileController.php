<?php

namespace App\Http\Controllers;

use App\Models\File;
use App\Models\User;
use Blessing\Filter;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class FileController extends Controller
{
    public function rename(Request $request, Dispatcher $dispatcher, Filter $filter)
    {
        $data = $request->validate([
            'id' => 'required',
            'name' => 'required|string|max:255',
            'location' => 'required|integer',
        ]);
        $name = $data['name'];

        /** @var File|null */
        $file = File::find($data['id']);
        if (empty($file)) {
            return response()->json(['code' => 1, 'message' => '文件不存在']);
        }

        if ($file->user_id !== auth()->id()) {
            return response()->json(['code' => 1, 'message' => '您没有操作的权限']);
        }

        $name = $filter->apply('file_rename', $name, [$file]);
        if ($file->type === 'directory') {
            $dispatcher->dispatch('directory.rename.before', [$file, /* new name */ $name]);
        } else {
            $dispatcher->dispatch('file.rename.before', [$file, /* new name */ $name]);
        }

        if (
            $file->name !== $name &&
            File::where('parent', $data['location'])
                ->where('name', $name)
                ->count() > 0
        ) {
            return response()->json([
                'code' => 1,
                'message' => '当前文件夹下已存在名为 '.$name.' 的文件或文件夹',
            ]);
        }

        $oldName = $file->name;
        $file->name = $name;
        $file->save();

        if ($file->type === 'directory') {
            $dispatcher->dispatch('directory.rename.after', [$file, $oldName]);
        } else {
            $dispatcher->dispatch('file.rename.after', [$file, $oldName]);
        }

        return response()->json(['code' => 0, 'message' => '重命名成功']);
    }

    public function upload(Request $request, Dispatcher $dispatcher, Filter $filter)
    {
        /** @var User */
        $user = auth()->user();
        $location = (int) $request->input('location');
        $file = $filter->apply('uploaded_file', $request->file('file'));
        $name = $filter->apply('uploaded_file_name', $file->getClientOriginalName(), [$file]);

        $parent = File::find($location);
        if (empty($parent)) {
            return response()->json(['code' => 1, 'message' => '无效位置'], 403);
        }
        if ($parent->user_id !== $user->id) {
            return response()->json(['code' => 1, 'message' => '您没有操作的权限'], 403);
        }

        $dispatcher->dispatch('file.upload.before', [$file, $name]);

        if (
            File::where('parent', $location)
                ->where('name', $name)
                ->count() > 0
        ) {
            return response()->json([
                'code' => 1,
                'message' => '当前文件夹下已存在名为 '.$name.' 的文件或文件夹',
            ], 403);
        }

        if ($user->total_space - $user->used_space - $file->getSize() < 0) {
            return response()->json([
                'code' => 1,
                'message' => '您的存储空间不足',
            ], 403);
        }

        $user->used_space += $file->getSize();
        $user->save();

        $hash = hash_file('sha256', $file->getPathname());
        $dup = File::where('sha256', $hash)->first();
        if (empty($dup)) {
            $path = $file->store('usr');
        } else {
            $path = $dup->path;
        }

        $model = new File();
        $model->name = $file->getClientOriginalName();
        $model->type = $file->getClientMimeType();
        $model->parent = $location;
        $model->path = $path;
        $model->sha256 = $hash;
        $model->size = $file->getSize();
        $model->user_id = $user->id;
        $model->save();

        $dispatcher->dispatch('file.upload.after', [$file, $model]);

        return response()->json([
            'code' => 0,
            'message' => '上传成功',
            'file' => $model,
        ]);
    }

    public function download(File $file, Dispatcher $dispatcher, Filter $filter)
    {
        abort_unless($file->user_id === auth()->id(), 403, '您没有操作的权限');
        abort_unless(Storage::exists($file->path), 404, '文件不存在');

        $name = $filter->apply('download_file_name', $file->name, [$file]);
        $dispatcher->dispatch('file.download', [$file, $name]);

        return Storage::download($file->path, $name, [
            'Content-Type' => $file->type,
        ]);
    }

    public function rm(Request $request, Dispatcher $dispatcher)
    {
        $data = $request->validate(['id' => 'required|integer']);
        $id = $data['id'];
        /** @var User */
        $user = auth()->user();

        /** @var File|null */
        $item = File::find($id);
        if (empty($item)) {
            return response()->json(['code' => 1, 'message' => '文件或文件夹不存在']);
        }
        if ($item->user_id !== $user->id) {
            return response()->json(['code' => 1, 'message' => '您没有操作的权限']);
        }

        if ($item->type === 'directroy') {
            $dispatcher->dispatch('directory.remove.before', [$item]);
            $dispatcher->dispatch('directory.delete.before', [$item]);
        } else {
            $dispatcher->dispatch('file.remove.before', [$item]);
            $dispatcher->dispatch('file.delete.before', [$item]);
        }

        $size = File::recursiveSize($item);
        $user->used_space -= $size;
        $user->save();
        $item->delete();

        if ($item->type === 'directroy') {
            $dispatcher->dispatch('directory.remove.after', [$item]);
            $dispatcher->dispatch('directory.delete.after', [$item]);
        } else {
            $dispatcher->dispatch('file.remove.after', [$item]);
            $dispatcher->dispatch('file.delete.after', [$item]);
        }

        return response()->json([
            'code' => 0,
            'message' => '已移至回收站',
            'size' => $size,
        ]);
    }

    public function cp(Request $request, Dispatcher $dispatcher)
    {
        $data = $request->validate([
            'id' => 'required|integer',
            'destination' => 'required|integer',
        ]);
        $user = auth()->user();

        /** @var File|null */
        $file = File::find($data['id']);
        if (empty($file)) {
            return response()->json(['code' => 1, 'message' => '文件不存在']);
        }
        if ($file->user_id !== $user->id) {
            return response()->json(['code' => 1, 'message' => '您没有操作的权限']);
        }

        /** @var File|null */
        $destination = File::find($data['destination']);
        if (empty($destination)) {
            return response()->json(['code' => 1, 'message' => '目标位置不存在']);
        }
        if ($destination->user_id !== $user->id) {
            return response()->json(['code' => 1, 'message' => '您没有操作的权限']);
        }

        if ($file->type === 'directory') {
            $dispatcher->dispatch('directory.copy.before', [$file, $destination]);
        } else {
            $dispatcher->dispatch('file.copy.before', [$file, $destination]);
        }

        $name = $file->name;
        if (
            File::where('parent', $data['destination'])
                ->where('name', $name)
                ->count() > 0
        ) {
            return response()->json([
                'code' => 1,
                'message' => '目标文件夹下已存在名为 '.$name.' 的文件或文件夹',
            ]);
        }

        $this->copyRecursive($file, $destination, $user);
        $user->save();

        if ($file->type === 'directory') {
            $dispatcher->dispatch('directory.copy.after', [$file, $destination]);
        } else {
            $dispatcher->dispatch('file.copy.after', [$file, $destination]);
        }

        return response(['code' => 0, 'message' => '复制成功']);
    }

    protected function copyRecursive(File $src, File $dst, User &$user)
    {
        $copied = new File();
        $copied->name = $src->name;
        $copied->type = $src->type;
        $copied->parent = $dst->id;
        $copied->path = $src->path;
        $copied->sha256 = $src->sha256;
        $copied->size = $src->size;
        $copied->user_id = $src->user_id;
        $copied->save();

        $user->used_space += $copied->size;

        $src->children->each(function (File $child) use ($copied, &$user) {
            $this->copyRecursive($child, $copied, $user);
        });
    }

    public function mv(Request $request, Dispatcher $dispatcher)
    {
        $data = $request->validate([
            'id' => 'required|integer',
            'destination' => 'required|integer',
        ]);

        /** @var File|null */
        $file = File::find($data['id']);
        if (empty($file)) {
            return response()->json(['code' => 1, 'message' => '文件不存在']);
        }
        if ($file->user_id !== auth()->id()) {
            return response()->json(['code' => 1, 'message' => '您没有操作的权限']);
        }

        /** @var File|null */
        $destination = File::find($data['destination']);
        if (empty($destination)) {
            return response()->json(['code' => 1, 'message' => '目标位置不存在']);
        }
        if ($destination->user_id !== auth()->id()) {
            return response()->json(['code' => 1, 'message' => '您没有操作的权限']);
        }

        if ($file->type === 'directory') {
            $dispatcher->dispatch('directory.move.before', [$file, $destination]);
        } else {
            $dispatcher->dispatch('file.move.before', [$file, $destination]);
        }

        $name = $file->name;
        if (
            File::where('parent', $data['destination'])
                ->where('name', $name)
                ->count() > 0
        ) {
            return response()->json([
                'code' => 1,
                'message' => '目标文件夹下已存在名为 '.$name.' 的文件或文件夹',
            ]);
        }

        $file->parent = (int) $data['destination'];
        $file->save();

        if ($file->type === 'directory') {
            $dispatcher->dispatch('directory.move.after', [$file, $destination]);
        } else {
            $dispatcher->dispatch('file.move.after', [$file, $destination]);
        }

        return response(['code' => 0]);
    }
}
