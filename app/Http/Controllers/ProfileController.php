<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    public function email(Request $request, Dispatcher $dispatcher)
    {
        $data = $request->validate([
            'email' => 'required|email',
            'password' => 'required|string',
        ]);

        /** @var \App\Models\User */
        $user = auth()->user();

        $dispatcher->dispatch('user.email.update.before', [$user, $data['email']]);

        if (!Hash::check($data['password'], $user->getAuthPassword())) {
            return response()->json(['code' => 1, 'message' => '密码不正确']);
        }

        $oldEmail = $user->email;
        $user->email = $data['email'];
        $user->save();

        $dispatcher->dispatch('user.email.update.after', [$user, $oldEmail]);

        return response()->json(['code' => 0, 'message' => '邮箱地址修改成功']);
    }

    public function name(Request $request, Dispatcher $dispatcher)
    {
        $data = $request->validate([
            'name' => 'required|string|max:30',
        ]);

        /** @var \App\Models\User */
        $user = auth()->user();

        $dispatcher->dispatch('user.name.update.before', [$user, $data['name']]);

        $oldName = $user->name;
        $user->name = $data['name'];
        $user->save();

        $dispatcher->dispatch('user.name.update.after', [$user, $oldName]);

        return response()->json(['code' => 0, 'message' => '用户名修改成功']);
    }

    public function password(Request $request, Dispatcher $dispatcher)
    {
        $data = $request->validate(([
            'old' => 'required|string',
            'password' => 'required|string|min:8|max:64|confirmed',
        ]));

        /** @var \App\Models\User */
        $user = auth()->user();

        $dispatcher->dispatch('user.password.update.before', [$user]);

        if (!Hash::check($data['old'], $user->getAuthPassword())) {
            return response()->json(['code' => 1, 'message' => '旧密码不正确']);
        }

        $user->password = Hash::make($data['password']);
        $user->save();

        $dispatcher->dispatch('user.password.update.after', [$user]);

        return response()->json(['code' => 0, 'message' => '密码修改成功']);
    }
}
