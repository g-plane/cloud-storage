<?php

namespace App\Http\Controllers;

use App\Models\File;
use App\Models\User;
use App\Services\FileUtils;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Http\Request;

class TrashController extends Controller
{
    public function list(Dispatcher $dispatcher)
    {
        $dispatcher->dispatch('trash.list');

        return File::onlyTrashed()
            ->where('user_id', auth()->id())
            ->orderBy('deleted_at')
            ->get();
    }

    public function restore(Request $request, Dispatcher $dispatcher)
    {
        $data = $request->validate(['id' => 'required|integer']);
        $id = $data['id'];
        /** @var User */
        $user = auth()->user();

        /** @var File|null */
        $item = File::onlyTrashed()->where('id', $id)->first();
        if (empty($item)) {
            return response()->json(['code' => 1, 'message' => '文件或文件夹不存在']);
        }
        if ($item->user_id !== $user->id) {
            return response()->json(['code' => 1, 'message' => '您没有操作的权限']);
        }

        $dispatcher->dispatch('trash.restore.before', [$item]);

        $size = File::recursiveSize($item);
        if ($user->used_space + $size > $user->total_space) {
            return response()->json(['code' => 1, 'message' => '您的存储空间不足']);
        }

        $item->restore();
        $user->used_space += $size;
        $user->save();

        $dispatcher->dispatch('trash.restore.after', [$item]);

        return response()->json([
            'code' => 0,
            'message' => '已恢复',
            'size' => $size,
        ]);
    }

    public function forceDelete(Request $request, Dispatcher $dispatcher)
    {
        $data = $request->validate(['id' => 'required|integer']);
        $id = $data['id'];

        /** @var File|null */
        $item = File::onlyTrashed()->where('id', $id)->first();
        if (empty($item)) {
            return response()->json(['code' => 1, 'message' => '文件或文件夹不存在']);
        }
        if ($item->user_id !== auth()->id()) {
            return response()->json(['code' => 1, 'message' => '您没有操作的权限']);
        }

        $dispatcher->dispatch('trash.delete.before', [$item]);
        $dispatcher->dispatch('trash.remove.before', [$item]);

        FileUtils::forceDeleteRecursive($item);

        $dispatcher->dispatch('trash.delete.after', [$item]);
        $dispatcher->dispatch('trash.remove.after', [$item]);

        return response()->json(['code' => 0, 'message' => '删除成功']);
    }

    public function clearAll(Dispatcher $dispatcher)
    {
        $dispatcher->dispatch('trash.clear-all.before');

        File::onlyTrashed()
            ->where('user_id', auth()->id())
            ->get()
            ->each(function (File $item) {
                $this->deleteRecursive($item);
            });

        $dispatcher->dispatch('trash.clear-all.after');

        return response(['code' => 0]);
    }
}
