<?php

namespace App\Http\Controllers;

use App\Services\PluginManager;
use Illuminate\Support\Arr;

class PluginsManagementController extends Controller
{
    public function list(PluginManager $pluginManager)
    {
        return $pluginManager
            ->all()
            ->map(function ($plugin) use ($pluginManager) {
                $name = $plugin->name;
                $manifest = $pluginManager->getManifest($name);

                $plugin->title = Arr::get($manifest, 'extra.plugin.title', $name);

                return $plugin;
            });
    }

    public function enable(PluginManager $pluginManager, $name)
    {
        $pluginManager->enable($name);

        return response()->json(['code' => 0, 'message' => '插件已开启']);
    }

    public function disable(PluginManager $pluginManager, $name)
    {
        $pluginManager->disable($name);

        return response()->json(['code' => 0, 'message' => '插件已关闭']);
    }

    public function delete(PluginManager $pluginManager, $name)
    {
        $pluginManager->delete($name);

        return response()->json(['code' => 0, 'message' => '插件已删除']);
    }
}
