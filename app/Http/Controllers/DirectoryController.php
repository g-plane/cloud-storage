<?php

namespace App\Http\Controllers;

use App\Models\File;
use Blessing\Filter;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Http\Request;

class DirectoryController extends Controller
{
    public function ls(Request $request, Dispatcher $dispatcher, Filter $filter)
    {
        $location = $request->input('location');

        $dispatcher->dispatch('directory.list', [$location]);

        [$directories, $files] = File::where('parent', $location)
            ->where('user_id', auth()->id())
            ->orderBy('name')
            ->get()
            ->partition(function (File $item) {
                return $item->type === 'directory';
            });
        $list = $filter->apply('list_directory', $directories->merge($files));

        return $list;
    }

    public function lst(Request $request, Dispatcher $dispatcher)
    {
        $root = File::find($request->query('root'));

        $dispatcher->dispatch('directory.list-tree', [$root]);

        return $this->listRecursive($root)->directories;
    }

    protected function listRecursive(File &$directory)
    {
        $directory->directories = $directory
            ->children
            ->filter(function (File $item) {
                return $item->type === 'directory';
            })
            ->map(function (File $child) {
                return $this->listRecursive($child);
            })
            ->map(function (File $directory) {
                return $directory->makeHidden([
                    'path',
                    'sha256',
                    'type',
                    'size',
                    'parent',
                    'user_id',
                    'children',
                    'created_at',
                    'updated_at',
                ]);
            })
            ->sortBy('name')
            ->values();

        return $directory->makeHidden([
            'path',
            'sha256',
            'type',
            'size',
            'parent',
            'user_id',
            'children',
            'created_at',
            'updated_at',
        ]);
    }

    public function mkdir(Request $request, Dispatcher $dispatcher, Filter $filter)
    {
        $data = $request->validate([
            'name' => 'required|string',
            'location' => 'required|integer',
        ]);
        $name = $filter->apply('mkdir_name', $data['name']);
        $location = (int) $data['location'];

        $dispatcher->dispatch('directory.create.before', [$name, $location]);

        $parent = File::find($location);
        if (empty($parent)) {
            return response()->json(['code' => 1, 'message' => '无效位置']);
        }
        if ($parent->user_id !== auth()->id()) {
            return response()->json(['code' => 1, 'message' => '您没有操作的权限']);
        }

        if (
            File::where('parent', $location)
                ->where('name', $name)
                ->count() > 0
        ) {
            return response()->json([
                'code' => 1,
                'message' => '当前文件夹下已存在名为 '.$name.' 的文件或文件夹',
            ]);
        }

        $directory = new File();
        $directory->name = $name;
        $directory->type = 'directory';
        $directory->parent = $location;
        $directory->size = 0;
        $directory->user_id = auth()->id();
        $directory->save();

        $dispatcher->dispatch('directory.create.after', [$directory]);

        return response()->json(['code' => 0, 'message' => '文件夹创建成功', 'dir' => $directory]);
    }
}
