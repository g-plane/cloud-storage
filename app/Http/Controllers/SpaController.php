<?php

namespace App\Http\Controllers;

use Blessing\Filter;

class SpaController extends Controller
{
    public function app(Filter $filter)
    {
        $scripts = $filter->apply('javascript', []);

        return view('index', compact('scripts'));
    }

    public function user()
    {
        return response()->json(auth()->user());
    }
}
