<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CheckRole
{
    public function handle(Request $request, Closure $next, $role)
    {
        abort_if($request->user()->role !== $role, 403);

        return $next($request);
    }
}
