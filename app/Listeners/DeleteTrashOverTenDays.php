<?php

namespace App\Listeners;

use App\Models\File;
use App\Services\FileUtils;

class DeleteTrashOverTenDays
{
    public function handle()
    {
        File::onlyTrashed()
            ->where('user_id', auth()->id())
            ->get()
            ->filter(function (File $item) {
                return $item->deleted_at < now()->subDays(10);
            })
            ->each(function (File $item) {
                FileUtils::forceDeleteRecursive($item);
            });
    }
}
