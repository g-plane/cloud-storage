<?php

namespace App\Listeners;

use Illuminate\Support\Facades\File;

class CopyPluginAssets
{
    public function handle($plugin)
    {
        $dir = public_path('plugins/'.$plugin->name);
        File::deleteDirectory($dir);

        File::copyDirectory(
            base_path('plugins/'.$plugin->name.'/assets'),
            $dir.'/assets'
        );
    }
}
