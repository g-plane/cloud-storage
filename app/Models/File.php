<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property string $name
 * @property string $type
 * @property int $parent
 * @property string $path
 * @property string $sha256
 * @property int $size
 * @property int $user_id
 * @property User $owner
 * @property Collection $children
 */
class File extends Model
{
    use SoftDeletes;

    protected $casts = [
        'parent' => 'integer',
        'size' => 'integer',
        'user_id' => 'integer',
    ];

    public function owner()
    {
        return $this->belongsTo(User::class);
    }

    public function children()
    {
        return $this->hasMany(File::class, 'parent');
    }

    public static function recursiveSize(File $item): int
    {
        if ($item->type === 'directory') {
            return $item->children->map(function (File $item) {
                return File::recursiveSize($item);
            })->sum();
        } else {
            return $item->size;
        }
    }
}
