<?php

namespace App\Models;

use App\Models\File;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

/**
 * @property int $id
 * @property string $email
 * @property string $name
 * @property int $used_space
 * @property int $total_space
 * @property string $role
 * @property int $storage_id
 * @property File $root
 */
class User extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'name', 'email', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'used_space' => 'integer',
        'total_space' => 'integer',
    ];

    public function getRootAttribute()
    {
        return File::find($this->storage_id);
    }
}
