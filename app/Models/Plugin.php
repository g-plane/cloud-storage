<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @property string $version
 * @property bool $is_enabled
 */
class Plugin extends Model
{
    protected $casts = [
        'is_enabled' => 'boolean',
    ];
}
